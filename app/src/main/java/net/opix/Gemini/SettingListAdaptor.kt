package net.opix.Gemini

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup
import android.widget.*

import net.opix.database.GeminiSetting
import net.opix.Gemini.GeminiApplication.Companion.ALPHA_07
import java.util.ArrayList

class SettingListAdaptor(protected var mContext: Context, objects: ArrayList<GeminiSetting>, private val mOnUserInteraction: SettingListAdaptor.OnUserInteraction) : BaseAdapter() {
    private var settings = ArrayList<GeminiSetting>()
    init {
        settings = objects
    }

    interface OnUserInteraction {
        fun onSwitchChanged(position: Int, option: Int, isChecked: Boolean)
        fun changeBackgroundImage()
    }

    override fun getItem(position: Int): GeminiSetting {
        return settings[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {

        return settings.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var conView = convertView
        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (conView == null)
            conView = inflater.inflate(R.layout.activity_single_row_1icon, parent, false)

        val display1    = conView!!.findViewById<View>(R.id.display1) as TextView

        display1.text   = mContext.getString(settings[position].mTitle)
        val color       = if (settings[position].choice || !settings[position].mUseSwitch) R.color.colorPrimaryDark else R.color.halfGray
        display1.setTextColor(ResourcesCompat.getColor(mContext.resources, color, null))

        val choice          = conView.findViewById<View>(R.id.choice) as Switch
        choice.isChecked    = settings[position].choice
        choice.tag          = settings[position].sharedPrefId
        choice.visibility   = if (settings[position].mUseSwitch) View.VISIBLE else View.GONE

        // Handle switch changes to change the favorited state
        choice.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { _, isChecked ->
            if (getItem(position).getChoice() == isChecked)
                return@OnCheckedChangeListener

            setChoice(position, isChecked)
            mOnUserInteraction.onSwitchChanged(position, settings[position].mTitle, isChecked)
        })

        val imageId = conView.findViewById<View>(R.id.imageId) as ImageView
        imageId.setImageResource(settings[position].mImageId)
        imageId.alpha = ALPHA_07

        display1.setOnClickListener { _ ->
            if (getItem(position).mImageId == R.drawable.ic_photo && getItem(position).getChoice()!!) {
                mOnUserInteraction.changeBackgroundImage()
            }
        }

       return conView
    }

    private fun setChoice(position: Int, isChecked: Boolean) {
        getItem(position).choice = isChecked
        notifyDataSetChanged()
    }
}
