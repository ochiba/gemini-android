package net.opix.Gemini

import android.content.Intent
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.os.Bundle
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_race_list.*

import net.opix.database.Event
import net.opix.database.EventSubrace

import org.json.JSONArray
import java.util.ArrayList
import org.json.JSONException

class RaceListActivity : GeminiActivity(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private lateinit var adapter: RaceListAdaptor
    private var postId: Int = 0
    private var selectedEvent: Event? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        selectedEvent = intent.getSerializableExtra("gemini_selected_event") as Event

        if (selectedEvent != null) {
            postId = selectedEvent!!.id
            title = selectedEvent!!.postTitle
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_race_list)

        supportActionBar?.setDisplayShowHomeEnabled(true)

        adapter = RaceListAdaptor(this, ArrayList<EventSubrace>())
        list.adapter = adapter

        swipe_refresh_layout.setOnRefreshListener(this)

        swipe_refresh_layout.post {
            swipe_refresh_layout.isRefreshing = true

            fetchSubraces()
        }
        list.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            if (position == 0 || position == adapter.getTrueCount() + 1)
                return@OnItemClickListener

            var bSearch = true

            var oneEvent: EventSubrace? = null

            if (position > 0 && position <= adapter.getTrueCount())
                oneEvent = adapter.getItem(position) as EventSubrace

            if (position > adapter.getTrueCount() + 1) {
                oneEvent = adapter.getItem(position - 1 - adapter.getTrueCount()) as EventSubrace
                bSearch = false
            }

            val intent = Intent(this@RaceListActivity, if (bSearch) SearchableActivity::class.java else ChartActivity::class.java)

            oneEvent?.let {
                intent.putExtra("gemini_selected_table_name", it.table_name)
                intent.putExtra("gemini_selected_display_name", it.display_name)
                intent.putExtra("gemini_selected_event", selectedEvent)
                startActivity(intent)
            }
        }
    }

    override fun onRefresh() {
        fetchSubraces()
    }

    private fun fetchSubraces() {
        swipe_refresh_layout.isRefreshing = true
        WebCoordinator.handleRequest(WebApiType.subraces, postId.toString(),this)
    }

    override fun swipeRefreshLayout() {
        swipe_refresh_layout.isRefreshing = false
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {
        val newList = ArrayList<EventSubrace>()

        if (jArray.length() > 0) {

            for (i in 0 until jArray.length()) {
                try {
                    val jArray2 = jArray.getJSONArray(i)
                    val oneSubrace = EventSubrace(jArray2.getInt(0),
                            postId,
                            jArray2.getString(1),
                            jArray2.getString(2))

                    newList.add(oneSubrace)

                } catch (e: JSONException) {
                    newList.clear()

                    runOnUiThread {
                        showToast("Fail to download the list of races.  Please try again.")
                    }
                }
            }
        }

        runOnUiThread {
            if (jArray.length() == 0)
                showToast("Fail to download the list of races.  Please try again.")

            adapter.refresh(newList)
        }
    }
}
