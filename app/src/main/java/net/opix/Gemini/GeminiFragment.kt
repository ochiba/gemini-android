package net.opix.Gemini

import android.content.Context
import androidx.fragment.app.Fragment
import android.widget.Toast
import org.json.JSONArray

open class GeminiFragment : Fragment(), WebCoordinator.WebCoordinatorDelegate {

    override fun showToast(errorMessage: String, context: Context?) {

        if (context != null)
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {}
    override fun swipeRefreshLayout() {}

    override fun showError(errorId: Int) {}
}