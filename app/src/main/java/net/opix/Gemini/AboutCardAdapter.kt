package net.opix.Gemini

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.text.util.Linkify

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.TextView

class AboutCardAdapter(private val activity: Activity) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {

        return 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(activity).inflate(R.layout.activity_single_card, parent, false)
        return CardViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {

        val cardViewHolder = holder as CardViewHolder

        if (position == 0) {
            cardViewHolder.line2.visibility = View.VISIBLE
            cardViewHolder.line3.visibility = View.VISIBLE

            cardViewHolder.line2.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS
            cardViewHolder.line3.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS

            cardViewHolder.title.text = "Contact"
            cardViewHolder.line1.text = "Gemini Next"
            cardViewHolder.line2.text = "(619)363-1501"
            cardViewHolder.line3.text = "support@gemininext.com"
        } else if (position == 1) {
            cardViewHolder.line2.visibility = View.VISIBLE
            cardViewHolder.line3.visibility = View.VISIBLE

            cardViewHolder.line1.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS
            cardViewHolder.line2.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS
            cardViewHolder.line3.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS

            cardViewHolder.title.text = "Credits"
            cardViewHolder.line1.text = "\u2022 https://www.google.com/design/icons/index.html"
            cardViewHolder.line2.text = "\u2022 https://icons8.com"
            cardViewHolder.line3.text = "\u2022 https://github.com/PhilJay/MPAndroidChart"
        } else {
            cardViewHolder.line2.visibility = View.VISIBLE
            cardViewHolder.line3.visibility = View.VISIBLE

            cardViewHolder.line2.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS
            cardViewHolder.line3.autoLinkMask = Linkify.WEB_URLS or Linkify.EMAIL_ADDRESSES or Linkify.PHONE_NUMBERS

            cardViewHolder.title.text = "Developer"
            cardViewHolder.line1.text = "Osamu Chiba"
            cardViewHolder.line2.text = "(858)232-8806"
            cardViewHolder.line3.text = "www.opix.net"
        }
    }

    private inner class CardViewHolder internal constructor(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        internal var title: TextView
        internal var line1: TextView
        internal var line2: TextView
        internal var line3: TextView

        init {
            title = v.findViewById<View>(R.id.title) as TextView
            line1 = v.findViewById<View>(R.id.line1) as TextView
            line2 = v.findViewById<View>(R.id.line2) as TextView
            line3 = v.findViewById<View>(R.id.line3) as TextView
        }
    }
}
