package net.opix.Gemini

import android.net.Uri
import android.content.Intent

import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.AdapterView
import android.widget.ListView
import java.util.Random

import com.facebook.cache.common.CacheKey
import com.facebook.cache.common.SimpleCacheKey
import com.facebook.drawee.backends.pipeline.Fresco

import com.facebook.drawee.backends.pipeline.PipelineDraweeController
import com.facebook.drawee.view.SimpleDraweeView

import com.facebook.imagepipeline.request.BasePostprocessor
import com.facebook.imagepipeline.request.ImageRequestBuilder

import net.opix.database.Event
import org.json.JSONArray
import java.util.ArrayList
import org.json.JSONException

class ResultsPageFragment : GeminiFragment(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private var swipeRefreshLayout: GeminiSwipeRefreshLayout? = null
    private var resultsPage: View? = null
    private lateinit var adapter: MainListAdaptor
    private var latesEventList: ArrayList<Event> = ArrayList()

    private val GEMINI = ResultsPageFragment::class.java.getSimpleName()

    private var mPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPage = arguments!!.getInt(RESULTS_PAGE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        resultsPage = inflater.inflate(R.layout.gemini_results_page, container, false)

        val listView = resultsPage!!.findViewById(android.R.id.list) as ListView
        swipeRefreshLayout = resultsPage!!.findViewById(R.id.swipe_refresh_layout) as GeminiSwipeRefreshLayout

        adapter = MainListAdaptor(requireActivity(), latesEventList)
        listView.adapter = adapter

        swipeRefreshLayout?.setOnRefreshListener(this)

        swipeRefreshLayout?.post {
            swipeRefreshLayout?.isRefreshing = true
            getBackgroundImageID()
        }

        listView.onItemClickListener = AdapterView.OnItemClickListener { _, view, position, _ ->
            // Do nothing if the first row (Latest Events) or the one between the latest and years lists.
            if (position == 0 || position == adapter.getEventCount() + 1)
                return@OnItemClickListener

            if (position > 0 && position <= adapter.getEventCount()) {
                val one_event = adapter.getEvent(position - 1)
                val intent = Intent(view.context, RaceListActivity::class.java)

                intent.putExtra("gemini_selected_event", one_event)
                startActivity(intent)
            } else {
                val selectedYear = adapter.getYear(position - adapter.getEventCount() - 2)
                val intent = Intent(view.context, MonthListActivity::class.java)

                intent.putExtra("gemini_selected_year", selectedYear)
                startActivity(intent)
            }
        }

        return swipeRefreshLayout
    }

    override fun onRefresh() {
        getBackgroundImageID()
    }

    private fun getBackgroundImageID() {

        GeminiApplication.instance?.let {

            // Make sure image uri exists every time the function is called.
            if (it.useOwnBackground() && it.checkImageExists())
                setBackgroundImage(it.backgroundUri)

            WebCoordinator.handleRequest(WebApiType.randomNumber, "", this)
        }
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {

        if (jArray.length() == 0)
            return

        if (type == WebApiType.randomNumber) {
            GeminiApplication.instance?.let {
                try {
                    if (!it.useOwnBackground()) {
                        val rand = Random()
                        val randomNum = rand.nextInt(jArray.length())
                        val uri = Uri.parse("https://gemininext.com/wp-content/plugins/GeminiRaceResults/images/bw_background/" + jArray.getString(randomNum))

                        setBackgroundImage(uri)
                    }

                    WebCoordinator.handleRequest(WebApiType.years, "", this)
                } catch (e: JSONException) {
                }
            }
        } else {
            fetchYears(jArray)
        }
    }

    private fun setBackgroundImage(uri: Uri?) {
        val draweeView = resultsPage!!.findViewById(R.id.gemini_background_image) as SimpleDraweeView

        val bwPostprocessor = object : BasePostprocessor() {
            override fun getName(): String {
                return "bwPostprocessor"
            }

            override fun getPostprocessorCacheKey(): CacheKey {
                return SimpleCacheKey(GeminiApplication.TAG)
            }
        }

        val request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setPostprocessor(bwPostprocessor)
                .build()

        val controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(draweeView.controller)
                // other setters as you need
                .build() as PipelineDraweeController

        draweeView.controller = controller
    }

    private fun fetchYears(jArray: JSONArray) {

        val newYearList = ArrayList<String>()
        val newEventList = ArrayList<Event>()

        try {
            // 1) Get years first.  It's a little ugly to dig out.
            val tempo = jArray.getJSONArray(0)

            for (j in 0 until tempo.length()) {
                val tempo2 = tempo.getJSONArray(j)

                if (tempo2.get(0) as String != "0000")
                    newYearList.add(tempo2.get(0) as String)
            }

            // 2) Get latest events next.
            for (i in 1 until jArray.length()) {
                val oneObject = jArray.getJSONObject(i)

                val one_event = Event(oneObject.getInt("ID"),
                        oneObject.getString("post_title"),
                        oneObject.getString("post_date"),
                        oneObject.getString("_grr_race_location_state"),
                        oneObject.getString("_grr_race_location_city"))
                newEventList.add(one_event)
            }

            activity?.let {
                if (!it.isFinishing && !it.isDestroyed) {
                    it.runOnUiThread {
                        adapter.refresh(newEventList, newYearList)
                    }
                }
            }
        } catch (e: JSONException) {
        }
    }
    override fun swipeRefreshLayout() {
        swipeRefreshLayout?.isRefreshing = false
    }

    companion object {

        val RESULTS_PAGE = "RESULTS_PAGE"

        fun newInstance(page: Int): ResultsPageFragment {
            val args = Bundle()
            args.putInt(RESULTS_PAGE, page)
            val fragment = ResultsPageFragment()
            fragment.arguments = args
            return fragment
        }
    }
}