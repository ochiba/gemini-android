package net.opix.Gemini

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import net.opix.database.Event
import org.json.JSONArray
import org.json.JSONException

import java.util.ArrayList

class ChartActivity : GeminiActivity(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private var tableName = ""
    private var selectedEvent: Event? = null
    private var displayTitle = ""
    private var mViewPager: GeminiViewPager? = null
    private var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val extras = intent.extras

        if (extras != null) {
            selectedEvent = intent.getSerializableExtra("gemini_selected_event") as Event
            tableName = intent.getStringExtra("gemini_selected_table_name")
            displayTitle = intent.getStringExtra("gemini_selected_display_name")

            title = "$displayTitle Charts"
        }
        else
            title = "Charts"

        // M = Marshmallow = 6
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
            setContentView(R.layout.activity_chart_m)
        else
            setContentView(R.layout.activity_chart)

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = findViewById(R.id.viewpager)
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout)

        supportActionBar?.setDisplayShowHomeEnabled(true)

        swipeRefreshLayout?.setOnRefreshListener(this)
        swipeRefreshLayout?.post {
            swipeRefreshLayout?.isRefreshing = true

            fetchColumn()
        }
    }

    private fun setViewPager() {
        // M = Marshmallow = 6
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            val tabLayout = findViewById<TabLayout>(R.id.sliding_tabs)
            tabLayout.setupWithViewPager(mViewPager)
            tabLayout.setSelectedTabIndicatorHeight(6)
        } else {
            val tabLayout = findViewById<SlidingTabLayout>(R.id.sliding_tabs)
            tabLayout.setDistributeEvenly(true)
            tabLayout.setViewPager(mViewPager)
        }
    }

    override fun onRefresh() {
        fetchColumn()
    }

    private fun fetchColumn() {
        // showing refresh animation before making http call
        swipeRefreshLayout?.isRefreshing = true

        WebCoordinator.handleRequest(WebApiType.columns, "$tableName&time_only=1",this)
    }

    override fun swipeRefreshLayout() {
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {

        val newChartList = ArrayList<String>()
        newChartList.add("Division")
        newChartList.add("Finish Time")

        if (jArray.length() > 0) {

            for (i in 0 until jArray.length()) {
                try {
                    val oneColumn = jArray.getString(i).toUpperCase()
                    val originalName = jArray.getString(i)

                    if (!oneColumn.equals("GUN", ignoreCase = true) &&
                            !oneColumn.equals("TOTAL TIME", ignoreCase = true) &&
                            !oneColumn.equals("CHIP", ignoreCase = true) &&
                            !oneColumn.contains("PACE") &&
                            !oneColumn.equals("T1", ignoreCase = true) &&
                            !oneColumn.equals("T2", ignoreCase = true))

                        newChartList.add(originalName)
                } catch (e: JSONException) {
                    newChartList.clear()
                    newChartList.add("Division")
                    newChartList.add("Finish Time")

                    runOnUiThread {
                        showToast("Fail to download the list of charts.  Please try again.")
                    }
                }
            }
        }

        // If more than 2 tabs, then Finish Time -> Finish
        if (newChartList.size > 2)
            newChartList[1] = "Finish"

        runOnUiThread {
            mViewPager!!.adapter = ChartFragmentPagerAdapter(supportFragmentManager, newChartList, tableName)
            setViewPager()
        }
    }
}
