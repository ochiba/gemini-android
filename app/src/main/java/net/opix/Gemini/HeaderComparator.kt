package net.opix.Gemini

import net.opix.database.QuickResult
import java.util.Comparator

/**
 * Created by osamuchiba on 7/3/15.
 */
class HeaderComparator : Comparator<QuickResult> {
    private var sortIndex = 0
    private var ascending: Boolean? = true
    private var isNumber: Boolean? = false

    fun setSortIndex(newIndex: Int) {
        sortIndex = newIndex
    }

    fun setIsNumber(newIsNumber: Boolean?) {
        isNumber = newIsNumber
    }

    fun setAscending(newAscending: Boolean?) {
        ascending = newAscending
    }

    override fun compare(result1: QuickResult, result2: QuickResult): Int {

        when (sortIndex) {
            R.id.header1 -> return if (isNumber!!) {
                if (ascending!!)
                    Integer.compare(Integer.parseInt(result1.display1), Integer.parseInt(result2.display1))
                else
                    Integer.compare(Integer.parseInt(result2.display1), Integer.parseInt(result1.display1))
            } else {
                if (ascending!!)
                    result1.display1.compareTo(result2.display1)
                else
                    result2.display1.compareTo(result1.display1)
            }

            R.id.header2 -> return if (isNumber!!) {
                if (ascending!!)
                    Integer.compare(Integer.parseInt(result1.display2), Integer.parseInt(result2.display2))
                else
                    Integer.compare(Integer.parseInt(result2.display2), Integer.parseInt(result1.display2))
            } else {
                if (ascending!!)
                    result1.display2.compareTo(result2.display2)
                else
                    result2.display2.compareTo(result1.display2)
            }
            R.id.header3 -> return if (isNumber!!) {
                if (ascending!!)
                    Integer.compare(Integer.parseInt(result1.display3), Integer.parseInt(result2.display3))
                else
                    Integer.compare(Integer.parseInt(result2.display3), Integer.parseInt(result1.display3))
            } else {
                if (ascending!!)
                    result1.display3.compareTo(result2.display3)
                else
                    result2.display3.compareTo(result1.display3)
            }
            R.id.header4 -> return if (isNumber!!) {
                if (ascending!!)
                    Integer.compare(Integer.parseInt(result1.display4), Integer.parseInt(result2.display4))
                else
                    Integer.compare(Integer.parseInt(result2.display4), Integer.parseInt(result1.display4))
            } else {
                if (ascending!!)
                    result1.display4.compareTo(result2.display4)
                else
                    result2.display4.compareTo(result1.display4)
            }
            R.id.header5 -> return if (isNumber!!) {
                if (ascending!!)
                    Integer.compare(Integer.parseInt(result1.display5), Integer.parseInt(result2.display5))
                else
                    Integer.compare(Integer.parseInt(result2.display5), Integer.parseInt(result1.display5))
            } else {
                if (ascending!!)
                    result1.display5.compareTo(result2.display5)
                else
                    result2.display5.compareTo(result1.display5)
            }
            R.id.overall -> return if (ascending!!)
                Integer.compare(result1.overall, result2.overall)
            else
                Integer.compare(result2.overall, result1.overall)
        }
        return 0
    }
}
