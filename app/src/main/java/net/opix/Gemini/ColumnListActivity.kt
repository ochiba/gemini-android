package net.opix.Gemini

import android.app.Activity
import android.content.Intent
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.os.Bundle

import android.view.MenuItem
import android.widget.AdapterView
import org.json.JSONException

import kotlinx.android.synthetic.main.activity_column_list.*
import org.json.JSONArray

class ColumnListActivity : GeminiActivity(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private var tableName = ""
    private var display3: String = ""
    private var display2: String = ""
    private var display1: String = ""
    private var display4: String = ""
    private var display5: String = ""
    private var columnCount = 3
    private lateinit var adapter: ColumnListAdaptor

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_column_list)

        val extras = intent.extras

        if (extras != null) {
            columnCount = intent.getIntExtra("gemini_column_count", columnCount)
            tableName = intent.getStringExtra("gemini_selected_table_name")
            display1 = intent.getStringExtra("gemini_display1")
            display2 = intent.getStringExtra("gemini_display2")
            display3 = intent.getStringExtra("gemini_display3")

            if (columnCount > 3) {
                display4 = intent.getStringExtra("gemini_display4")
                display5 = intent.getStringExtra("gemini_display5")
            }
        }

        title = "Columns (Select up to $columnCount)"

        adapter = ColumnListAdaptor(this, columnCount)

        // assign the list adapter
        list.adapter = adapter

        swipe_refresh_layout.setOnRefreshListener(this)
        swipe_refresh_layout.post {
            swipe_refresh_layout.isRefreshing = true

            fetchColumn()
        }

        list.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ -> adapter!!.setSelected(position) }

        // add back arrow to toolbar
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onRefresh() {
        fetchColumn()
    }

    private fun fetchColumn() {
        adapter.clear()

        swipe_refresh_layout.isRefreshing = true

        WebCoordinator.handleRequest(WebApiType.columns, tableName,this)
    }

    // @override
    override fun onBackPressed() {
        display1 = ""
        display2 = ""
        display3 = ""
        display4 = ""
        display5 = ""

        for (i in 0 until columnCount) {
            val selected = adapter.getSelectedColumn(i)

            // Once empty is found, then no need to go through the rest.
            if (selected.isEmpty())
                break

            if (i == 0)
                display1 = selected

            if (i == 1)
                display2 = selected

            if (i == 2)
                display3 = selected

            if (i == 3)
                display4 = selected

            if (i == 4)
                display5 = selected
        }

        val intent = Intent()

        intent.putExtra("gemini_display1", display1)
        intent.putExtra("gemini_display2", display2)
        intent.putExtra("gemini_display3", display3)
        intent.putExtra("gemini_display4", display4)
        intent.putExtra("gemini_display5", display5)
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun swipeRefreshLayout() {
        swipe_refresh_layout.isRefreshing = false
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {
        val newColumnSet = ArrayList<String>()

        if (jArray.length() > 0) {

            for (i in 0 until jArray.length()) {
                try {
                    val oneColumn = jArray.getString(i)

                    if (!oneColumn.equals("First Name", ignoreCase = true) &&
                            !oneColumn.equals("Last Name", ignoreCase = true) &&
                            !oneColumn.equals("Overall", ignoreCase = true) &&
                            !oneColumn.equals("Bib", ignoreCase = true) &&
                            !oneColumn.equals("State", ignoreCase = true) &&
                            !oneColumn.equals("Country", ignoreCase = true) &&
                            !oneColumn.equals("Total Runners", ignoreCase = true) &&
                            !oneColumn.equals("TOTAL COMPETITORS", ignoreCase = true) &&
                            !oneColumn.equals("Gender Total", ignoreCase = true) &&
                            !oneColumn.equals("Division Total", ignoreCase = true) &&
                            !oneColumn.equals("ID", ignoreCase = true))

                        newColumnSet.add(oneColumn)
                } catch (e: JSONException) {
                    showToast("Fail to download the list of columns.  Please try again.")
                    newColumnSet.clear()
                }
            }
        }

        runOnUiThread {
            if (jArray.length() == 0)
                showToast("Fail to download the list of columns.  Please try again.")

            adapter.addData(newColumnSet)

            adapter.addSelected(display1)
            adapter.addSelected(display2)
            adapter.addSelected(display3)

            if (columnCount > 3) {
                adapter.addSelected(display4)
                adapter.addSelected(display5)
            }

            adapter.notifyDataSetChanged()
        }
    }
}