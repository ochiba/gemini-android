package net.opix.Gemini

import android.Manifest
import android.app.Activity

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle

import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat

import androidx.appcompat.app.ActionBarDrawerToggle
import android.util.TypedValue
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import androidx.drawerlayout.widget.DrawerLayout

import android.text.Spanned
import android.text.SpannableStringBuilder
import android.widget.TextView

// icons
// https://www.google.com/design/icons/index.html
// Holo colors
// http://android-holo-colors.com
//
// Signing
// http://developer.android.com/tools/publishing/app-signing.html#studio
// Facebook
// https://developers.facebook.com/apps/476364249196384/settings/

class MainActivity : GeminiActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var currentPage = 0
    lateinit private var toggle: ActionBarDrawerToggle
    private var favoritesPageFragment: FavoritesPageFragment? = null

    private val PERMISSION_GALLERY = 2
    private val BACKGROUND_IMAGE = 4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            currentPage = savedInstanceState.getInt("currentSelection")
        }

        //Note to make drawer icon white, see @style/ToolbarColoredBackArrow
        setSupportActionBar(toolbar)

        if (supportActionBar != null)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toggle = object : ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {}

        drawer_layout.addDrawerListener(toggle)

        toggle.setToolbarNavigationClickListener({ onBackPressed() })
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        selectResultsPage(currentPage)

        // Only the number part = digit font.
        val header = nav_view.getHeaderView(0)
        val digitFont   = Typeface.createFromAsset(this.assets, "fonts/TickingTimebombBB.ttf")

        // 1 looks tight so add a space in front of it.
        val version     = String.format("Version %s", BuildConfig.VERSION_NAME).replace("1", " 1")
        val space       = version.indexOf(" ")

        var ss = SpannableStringBuilder(version)
        ss.setSpan(CustomTypefaceSpan(digitFont), space + 1, version.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        header.version.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
        header.version.setText(ss, TextView.BufferType.SPANNABLE)

        setTitle(R.string.race_list_adaptor_results)
    }

    fun selectResultsPage(index: Int) {
        currentPage = index
        onNavigationItemSelected(nav_view.getMenu().getItem(currentPage))
        nav_view.getMenu().getItem(currentPage).isChecked = true
    }

    fun checkGalleryPermission(selectImage: Boolean = false, requestPermission: Boolean = true) {

        val readStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

        if (!readStorage) {

            if (requestPermission)
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_GALLERY)
            else
                GeminiApplication.instance!!.setUseOwnBackground(false)
        } else {
            if (selectImage)
                setOwnBackgroundImage()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_GALLERY ->
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        setOwnBackgroundImage()
                    else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        GeminiApplication.instance!!.setUseOwnBackground(false)

                        if (currentPage == 2) {
                            // Refresh the Settings page.  But fake-select the first page to make it happen.
                            nav_view.getMenu().getItem(0).isChecked = true
                            selectResultsPage(2)
                        }
                    }
                }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        // Don't do anything if currently selected.
        if (item.isChecked) {
            drawer_layout.closeDrawer(GravityCompat.START)
            return true
        }

        var fragment: androidx.fragment.app.Fragment? = null

        when (item.itemId) {
            R.id.nav_races -> {
                if (GeminiApplication.instance!!.useOwnBackground() && GeminiApplication.instance!!.checkImageExists())
                    checkGalleryPermission(false, false)

                currentPage = 0
                toolbar.title = getString(R.string.race_list_adaptor_results)
                fragment = ResultsPageFragment.newInstance(item.itemId)
            }

            R.id.nav_favorites -> {
                currentPage = 1
                toolbar.title = getString(R.string.favorites)
                fragment = FavoritesPageFragment.newInstance(item.itemId)
                favoritesPageFragment = fragment
            }

            R.id.nav_settings -> {
                currentPage = 2
                toolbar.title = getString(R.string.action_settings)
                fragment = MorePageFragment.newInstance(item.itemId)
            }

            R.id.nav_about -> {
                currentPage = 3
                toolbar.title = getString(R.string.about)
                fragment = AboutFragment.newInstance(item.itemId)
            }
        }

        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.main_content, fragment)
            ft.commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (!toggle.isDrawerIndicatorEnabled()) {
                setDrawerState(true)

                if (favoritesPageFragment != null)
                    favoritesPageFragment!!.emptySelection()

                return
            }
            super.onBackPressed()
        }
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putInt("currentSelection", currentPage)
        super.onSaveInstanceState(savedInstanceState)
    }

    // https://stackoverflow.com/questions/19439320/disabling-navigation-drawer-toggling-home-button-up-indicator-in-fragments
    fun setDrawerState(isEnabled: Boolean) {
        if (isEnabled) {
            drawer_layout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNLOCKED)
            toggle.onDrawerStateChanged(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNLOCKED)
            toggle.setDrawerIndicatorEnabled(true)
            toolbar.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        } else {
            drawer_layout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            toggle.onDrawerStateChanged(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            toggle.setDrawerIndicatorEnabled(false)
            toolbar.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
        }
        toggle.syncState()
    }

    fun setOwnBackgroundImage() {
        val intent = Intent()
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)

        intent.type = "image/*"
        startActivityForResult(intent, BACKGROUND_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == BACKGROUND_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val uri = data.data

                // Need these 3 lines so that Uri can be used later, over and over.
                // Reference: https://stackoverflow.com/questions/25414352/how-to-persist-permission-in-android-api-19-kitkat
                val takeFlags = data.flags and Intent.FLAG_GRANT_READ_URI_PERMISSION
                val resolver = contentResolver
                resolver.takePersistableUriPermission(uri!!, takeFlags)

                GeminiApplication.instance!!.backgroundUri = uri
                showSnackbar(R.string.success)

                // Show the newly selected image, after 2 seconds.
                android.os.Handler().postDelayed(
                        {
                            selectResultsPage(0)
                        }, 2000)

            }
        }
    }

    private fun showSnackbar(stringId: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.drawer_layout),
                stringId, Snackbar.LENGTH_SHORT)

        // get snackbar view
        val snackbarView = snackbar.view

        // change snackbar text color
        val snackbarTextId = com.google.android.material.R.id.snackbar_text
        val textView = snackbarView.findViewById(snackbarTextId) as TextView
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.gemini_normal_font_size))
        textView.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary))

        snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        snackbarView.alpha = GeminiApplication.ALPHA_07
        snackbar.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Only handle with DrawerToggle if the drawer indicator is enabled.
        if (toggle.isDrawerIndicatorEnabled() && toggle.onOptionsItemSelected(item)) {
            return true
        }
        // Handle action buttons
        when (item.itemId) {
            // Handle home button in non-drawer mode
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}
