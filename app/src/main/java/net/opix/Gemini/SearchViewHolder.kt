package net.opix.Gemini

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView

class SearchViewHolder(private val viewLayout: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewLayout) {
    var container: RelativeLayout
    var counter: TextView
    var overall: TextView
    var name_bib: TextView
    var display1: TextView
    var display2: TextView
    var display3: TextView
    var display4: TextView? = null
    var display5: TextView? = null

    init {
        container = viewLayout.findViewById<View>(R.id.container) as RelativeLayout
        counter = viewLayout.findViewById<View>(R.id.counter) as TextView
        overall = viewLayout.findViewById<View>(R.id.overall) as TextView
        name_bib = viewLayout.findViewById<View>(R.id.athlete_name_bib) as TextView
        display1 = viewLayout.findViewById<View>(R.id.display1) as TextView
        display2 = viewLayout.findViewById<View>(R.id.display2) as TextView
        display3 = viewLayout.findViewById<View>(R.id.display3) as TextView

        display1.gravity = Gravity.CENTER
        display2.gravity = Gravity.CENTER
        display3.gravity = Gravity.CENTER

        display1.setTextColor(Color.DKGRAY)
        display2.setTextColor(Color.DKGRAY)
        display3.setTextColor(Color.DKGRAY)
        overall.setTextColor(Color.DKGRAY)
    }

    fun enableDisplay4and5() {
        display4 = viewLayout.findViewById<View>(R.id.display4) as TextView
        display5 = viewLayout.findViewById<View>(R.id.display5) as TextView

        display4?.gravity = Gravity.CENTER
        display5?.gravity = Gravity.CENTER

        display4?.setTextColor(Color.DKGRAY)
        display5?.setTextColor(Color.DKGRAY)
    }
}