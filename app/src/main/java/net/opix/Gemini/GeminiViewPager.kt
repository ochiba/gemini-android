package net.opix.Gemini

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class GeminiViewPager : androidx.viewpager.widget.ViewPager {

    private var enableSwipe: Boolean = false

    constructor(context: Context) : super(context) {
        enableSwipe = true
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        enableSwipe = true
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        // Never allow swiping to switch between pages
        return enableSwipe && super.onInterceptTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        // Never allow swiping to switch between pages
        return enableSwipe && super.onTouchEvent(event)
    }

    fun setEnableSwipe(enableSwipe: Boolean) {
        this.enableSwipe = enableSwipe
    }
}