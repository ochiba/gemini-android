package net.opix.Gemini

import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.utils.ViewPortHandler

class GeminiYAxisFormatter : IValueFormatter {

    override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {
        return String.format("%d", Math.round(value))
    }
}