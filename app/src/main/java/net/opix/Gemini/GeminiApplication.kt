package net.opix.Gemini

import android.app.Activity
import android.app.Application
import android.content.ClipData

import android.content.ClipboardManager
import android.content.Context
import android.content.Intent

import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AlertDialog
import android.text.TextUtils

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat

import java.util.Calendar
import java.util.Locale

import com.facebook.drawee.backends.pipeline.Fresco

// Fresco
// http://frescolib.org

class GeminiApplication : Application() {

    private var mRequestQueue: RequestQueue? = null
    private var mUseOwnBackground = false
    private var mBackgroundUri: Uri? = null

    private val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(applicationContext)
            }

            return mRequestQueue!!
        }

    var backgroundUri: Uri?
        get() = mBackgroundUri
        set(newBackgroundUri) {

            val sharedPref = applicationContext.getSharedPreferences(TAG, Context.MODE_PRIVATE)

            val editor = sharedPref.edit()
            editor.putString(GeminiApplication.PREF_BACKGROUND_IMAGE_URI, newBackgroundUri.toString())
            editor.commit()

            mBackgroundUri = newBackgroundUri
        }

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        instance = this

        val sharedPref = applicationContext.getSharedPreferences(TAG, Context.MODE_PRIVATE)

        mUseOwnBackground = sharedPref.getBoolean(PREF_BACKGROUND_IMAGE, false)

        if (mUseOwnBackground) {
            val uri = sharedPref.getString(PREF_BACKGROUND_IMAGE_URI, "")

            if (uri != null && !uri.isEmpty()) {

                // Check if the image file still exists.  If not, then set use own background = false.
                if (!checkImageExists(uri)) {
                    setUseOwnBackground(false)
                    return
                }

                mBackgroundUri = Uri.parse(uri)
            } else
                setUseOwnBackground(false)
        }
    }

    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        requestQueue.add(req)
    }

    fun cancelPendingRequests(tag: Any) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    // Convert MySQL Date ("yyyy-MM-dd HH:mm:ss") to US long format like "January 1, 2000".
    fun getPostDate(dateStr: String, format: String = "MMMM d, yyyy"): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val cal = Calendar.getInstance()

        try {
            val dateObj = sdf.parse(dateStr)
            cal.time = dateObj

            val nSDateFormatterLongStyle = SimpleDateFormat(format)
            return nSDateFormatterLongStyle.format(dateObj)
        } catch (e: ParseException) {
            // Oops
        }

        return ""
    }

    // http://stackoverflow.com/questions/3312935/nsnumberformatter-and-th-st-nd-rd-ordinal-number-endings
    fun ordinalNumberFormat(num: Int): String {
        val ending: String

        val ones = num % 10
        var tens = num / 10
        tens = tens % 10

        if (tens == 1) {
            ending = "th"
        } else {
            when (ones) {
                1 -> ending = "st"
                2 -> ending = "nd"
                3 -> ending = "rd"
                else -> ending = "th"
            }
        }

        return String.format("%d%s", num, ending)
    }

    fun shareMessage(message: String) {

        var manager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        manager.setPrimaryClip(ClipData.newPlainText("Race Results", message))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) { // At least KitKat
            val sendIntent = Intent(Intent.ACTION_SEND)
            sendIntent.type = "text/plain"
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Yo!  Check this out!")
            sendIntent.putExtra(Intent.EXTRA_TEXT, message)
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(sendIntent)
        } else {
            val smsIntent = Intent(android.content.Intent.ACTION_VIEW)
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("sms_body", message)
            startActivity(smsIntent)
        }
    }

    fun useOwnBackground(): Boolean {
        return mUseOwnBackground
    }

    fun setUseOwnBackground(newUseOwnBackground: Boolean, context: Context? = null) {

        val context2 = context ?: applicationContext

        if (context2 == null)
            return

        val sharedPref = context2.getSharedPreferences(TAG, Context.MODE_PRIVATE)

        val editor = sharedPref.edit()
        editor.putBoolean(GeminiApplication.PREF_BACKGROUND_IMAGE, newUseOwnBackground)

        if (!newUseOwnBackground)
            mBackgroundUri = null

        editor.commit()

        mUseOwnBackground = newUseOwnBackground
    }

    private fun checkImageExists(uri: String?): Boolean {
        if (uri.isNullOrEmpty())
            return false

        try {
            val inputStream = applicationContext.contentResolver.openInputStream(Uri.parse(uri))

            val bOK = inputStream != null
            inputStream!!.close()

            return bOK
        } catch (o: IOException) {
            return false
        }
    }

    fun checkImageExists(): Boolean {

        if (mBackgroundUri == null) {
            setUseOwnBackground(false)
            return false
        }

        val bOK = checkImageExists(mBackgroundUri?.toString())

        if (!bOK)
            setUseOwnBackground(bOK)

        return bOK
    }

    fun showAlertOKDialog(context: Context, message: Int) {
        // It If trying to show a dialog after execution of a background thread, while the Activity is being destroyed.
        if ((context as Activity).isFinishing)
            return

        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.app_name)
        builder.setMessage(message)
        builder.setCancelable(false)
        builder.setPositiveButton(R.string.ok) { _, _ -> }

        builder.create().show()
    }

    private fun getEntropy(s: String): Int {
        var entropy = 0
        val noSpace = s.replace(" ", "")

        for (i in 0 until noSpace.length)
            entropy += Character.getNumericValue(noSpace[i])

        return entropy
    }

    fun getBackgroundColor(context: Context?, s: String): String {

        val context2 = context ?: applicationContext

        val colorId = getEntropy(s)
        val allColors = context2.resources.getStringArray(R.array.profile_circle_colors)

        val index = colorId % allColors.size
        return allColors[index]
    }

    companion object {

        val TAG = GeminiApplication::class.java.getSimpleName()

        @get:Synchronized
        var instance: GeminiApplication? = null
            private set

        val PREF_BACKGROUND_IMAGE = "BackgroungImage"
        val PREF_BACKGROUND_IMAGE_URI = "BackgroungImageUri"

        val ALPHA_07 = 0.7f
    }
}
