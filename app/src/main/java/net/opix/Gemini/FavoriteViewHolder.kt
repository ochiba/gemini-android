package net.opix.Gemini

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

class FavoriteViewHolder(private val viewLayout: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewLayout) {
    var post_date: TextView
    var post_title: TextView
    var athlete: TextView
    var race: TextView
    var overall: TextView
    var container: LinearLayout
    var profileColor: TextView

    init {
        container = viewLayout.findViewById(R.id.container) as LinearLayout
        post_date = viewLayout.findViewById<View>(R.id.post_date) as TextView
        post_title = viewLayout.findViewById<View>(R.id.post_title) as TextView
        athlete = viewLayout.findViewById<View>(R.id.athlete_name_bib) as TextView
        race = viewLayout.findViewById<View>(R.id.race) as TextView
        overall = viewLayout.findViewById<View>(R.id.overall) as TextView
        profileColor = viewLayout.findViewById<View>(R.id.profile_circle) as TextView
    }

    fun getAllElements(): Array<View> {
        return arrayOf(container, post_date, post_title, athlete, race, overall, profileColor)
    }
}