package net.opix.Gemini

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId

import android.content.ContentValues.TAG
import com.google.firebase.messaging.FirebaseMessagingService

class GeminiFirebase : FirebaseMessagingService() {

    val token: String?
        get() = FirebaseInstanceId.getInstance().token


    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(TAG, "Refreshed token: " + refreshedToken!!)

        // TODO: Implement this method to send any registration to your app's servers.
        //        sendRegistrationToServer(refreshedToken);
    }
}