package net.opix.Gemini

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import net.opix.database.Bookmark
import net.opix.database.BookmarkWrapper

import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    internal var mList = ArrayList<Bookmark>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val extras = intent.extras

        if (extras != null) {
            val wrapper = extras.getSerializable("bookmarkList") as BookmarkWrapper
            mList = wrapper.bookmarkList
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        title = "Race Map"
    }

    override fun onMapReady(map: GoogleMap) {

        for (i in mList.indices) {
            val oneBookmark = mList[i]

            var city = oneBookmark.city.replace(" ".toRegex(), "%20")
            city += "%20"
            city += oneBookmark.state

            var url = "http://maps.google.com/maps/api/geocode/json?address=$city&sensor=false&key"
            url += getString(R.string.google_api_key)

            val req = JsonObjectRequest(Request.Method.GET, url, null, Response.Listener { response ->
                val location: JSONObject
                try {
                    // Get JSON Array called "results" and then get the 0th
                    // complete object as JSON
                    location = response.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location")
                    map.addMarker(MarkerOptions().position(LatLng(location.getDouble("lat"), location.getDouble("lng"))).title("Marker"))

                    if (i == mList.size - 1) {
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.getDouble("lat"), location.getDouble("lng")), 7.0f))
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error -> Log.d("Error.Response", error.toString()) })

            GeminiApplication.instance!!.addToRequestQueue(req)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}
