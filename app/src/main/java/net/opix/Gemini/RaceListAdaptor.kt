package net.opix.Gemini

import android.app.Activity
import android.content.Context
import android.graphics.Color
import androidx.core.content.res.ResourcesCompat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

import android.widget.TextView
import android.view.Gravity
import net.opix.database.EventSubrace
import java.util.ArrayList

class RaceListAdaptor(private val activity: Activity, private val raceList: ArrayList<EventSubrace>) : BaseAdapter() {
    private var inflater: LayoutInflater? = null

    override fun getCount(): Int {
        return raceList.size * 2 + 2
    } // one for "Individual Results Events", one for Charts.

    fun getTrueCount(): Int {
        return raceList.size
    }

    override fun getItem(location: Int): Any? {

        if (location > 0 && location <= raceList.size)
            return raceList[location - 1]

        return if (location > raceList.size + 1) raceList[location - 2 - raceList.size] else null
    }

    override fun getItemId(position: Int): Long {

        if (position == 0 || position == raceList.size + 1)
            return 0

        val oneEvent = getItem(position) as EventSubrace?
        return oneEvent?.ID?.toLong() ?: 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var conView = convertView

        if (inflater == null)
            inflater = activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (conView == null)
            conView = inflater!!.inflate(R.layout.activity_single_row, parent, false)

        val one_row = conView!!.findViewById<View>(R.id.display1) as TextView
        one_row.gravity = Gravity.CENTER

        if (position == 0 || position == raceList.size + 1) {

            one_row.setTextColor(Color.DKGRAY)

            // Right now nothing between Latest Events and Years.
            if (position == 0)
                one_row.setText(R.string.race_list_adaptor_results)
            else
                one_row.setText(R.string.race_list_adaptor_charts)

            // Entire row's background is light gray
            conView.setBackgroundColor(ResourcesCompat.getColor(activity.resources, R.color.lightLightGray, null))
        } else {
            val one_event = getItem(position) as EventSubrace?
            one_row.setTextColor(Color.BLACK)
            one_row.text = one_event!!.display_name

            conView.setBackgroundColor(Color.WHITE)
        }
        return conView
    }

    fun refresh(newList : ArrayList<EventSubrace>) {
        raceList.clear()
        raceList.addAll(newList)
        notifyDataSetChanged()
    }
}
