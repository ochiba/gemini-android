package net.opix.Gemini

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet

import android.view.View
import android.widget.Button

/**
 * Created by osamuchiba on 7/3/15.
 * http://developer.android.com/training/custom-views/create-view.html
 */
class HeaderButton(context: Context, attrs: AttributeSet) : Button(context, attrs) {

    var ascending: Boolean = false
        private set

    init {

        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.HeaderButton,
                0, 0)
        try {
            ascending = a.getBoolean(R.styleable.HeaderButton_ascending, false)
        } finally {
            a.recycle()
        }
    }

    fun setAscending(newAscednding: Boolean, setBold: Boolean?) {
        ascending = newAscednding

        if (setBold!!)
        // Make bold.
            setTypeface(typeface, Typeface.BOLD)
        else
        // Remove Bold from everything else.
        // http://stackoverflow.com/questions/6200533/set-textview-style-bold-or-italic
            setTypeface(Typeface.create(typeface, Typeface.NORMAL), Typeface.NORMAL)
    }

    fun toggleAscending() {
        ascending = !ascending
    }

    // If the new title is empty, the button itself is hidden.
    fun setTextAndVisibility(newText: String) {
        super.setText(newText)
        visibility = if (newText.isEmpty()) View.INVISIBLE else View.VISIBLE

        invalidate()
        requestLayout()
    }
}
