package net.opix.Gemini

import android.app.Activity
import android.content.Context
import android.graphics.Color
import androidx.core.content.res.ResourcesCompat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

import android.widget.TextView
import java.util.ArrayList

class ColumnListAdaptor(private val activity: Activity, newLimit: Int) : BaseAdapter() {
    private var inflater: LayoutInflater? = null
    private val columnlist: MutableList<String>
    private val selectedlist: MutableList<Int>
    private var limit = 3

    init {
        this.columnlist = ArrayList()
        this.selectedlist = ArrayList()
        limit = newLimit
    }

    override fun getCount(): Int {
        return columnlist.size
    }

    override fun getItem(location: Int): Any {
        return columnlist[location]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun clear() {
        columnlist.clear()
        selectedlist.clear()
    }

    fun getSelectedColumn(i: Int): String {

        if (i < selectedlist.size) {
            val index = selectedlist[i]
            return columnlist[index]
        }

        return ""
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var conView = convertView

        if (inflater == null)
            inflater = activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (conView == null)
            conView = inflater!!.inflate(R.layout.gemini_column_row_layout, parent, false)

        val oneColumn = conView!!.findViewById<View>(R.id.oneColumn) as TextView
        oneColumn.text = columnlist[position]

        val counter = conView.findViewById<View>(R.id.counter) as TextView
        // Empty out first.
        counter.text = ""

        for (i in selectedlist.indices) {
            val selectedIndex = selectedlist[i]

            if (selectedIndex == position) {
                counter.text = Integer.toString(i + 1)
                break
            }
        }

        // Change the color or row alternately.
        conView.setBackgroundColor(if (position % 2 == 0) ResourcesCompat.getColor(activity.resources, R.color.lightLightGray, null) else Color.WHITE)
        return conView
    }

    fun addData(newColumn: ArrayList<String>) {
        columnlist.addAll(newColumn)
    }

    fun addSelected(newColumn: String) {

        if (!newColumn.isEmpty()) {
            for (i in columnlist.indices) {
                val oneColumn = columnlist[i]

                if (oneColumn.equals(newColumn, ignoreCase = true)) {
                    if (selectedlist.size < limit)
                        selectedlist.add(i)

                    break
                }
            }
        }
    }

    fun setSelected(position: Int) {
        var bRemoved: Boolean = false

        for (j in selectedlist.indices) {
            val index = selectedlist[j]

            if (index == position) {
                selectedlist.removeAt(j)
                bRemoved = true
                break
            }
        }

        if (!bRemoved && selectedlist.size < limit)
            selectedlist.add(position)

        notifyDataSetChanged()
    }
}
