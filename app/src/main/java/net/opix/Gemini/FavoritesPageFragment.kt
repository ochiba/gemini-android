package net.opix.Gemini

import android.os.Bundle

import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.opix.database.Bookmark
import net.opix.database.GeminiDatabaseHelper
import java.util.ArrayList
import androidx.appcompat.app.AlertDialog

class FavoritesPageFragment : GeminiFragment(), FavoriteListAdaptor.OnFavoriteUserInteraction {

    private var adapter: FavoriteListAdaptor? = null
    private var bookmarkList = ArrayList<Bookmark>()
    private var recyclerView: androidx.recyclerview.widget.RecyclerView? = null
    private var mView: View? = null
    private var isTablet: Boolean = false
    private var mEmptyView: TextView? = null
    private var deleteMenu: MenuItem? = null
    private var mPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPage = arguments!!.getInt(FAVORITES_PAGE)
    }

    private fun fillBookmarkData() {
        bookmarkList.clear()
        bookmarkList.addAll(GeminiDatabaseHelper(mView!!.context).getFavorites())

        adapter?.notifyDataSetChanged()

        val isEmpty = bookmarkList.isEmpty()
        mEmptyView!!.visibility = if (isEmpty) View.VISIBLE else View.GONE
        recyclerView!!.visibility = if (isEmpty) View.GONE else View.VISIBLE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.activity_favorites_list, container, false)

        recyclerView = mView!!.findViewById<View>(R.id.recyclerView) as androidx.recyclerview.widget.RecyclerView

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(requireActivity())
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
        recyclerView!!.layoutManager = layoutManager
        mEmptyView = mView!!.findViewById(R.id.list_empty_view)

        isTablet = Integer.parseInt(recyclerView!!.tag.toString()) == 1
        adapter = FavoriteListAdaptor(requireActivity(), bookmarkList, isTablet, this)

        recyclerView!!.adapter = adapter
        setHasOptionsMenu(true)
        return mView
    }

    override fun onResume() {
        super.onResume()
        fillBookmarkData()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_favorites, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        deleteMenu = menu.getItem(0)
        deleteMenu?.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (bookmarkList.isEmpty()) {
            return false
        }

        if (item.itemId == R.id.delete_favorites) {

            val adb = AlertDialog.Builder(requireActivity())

            adb.setMessage(R.string.confirm_delete)
            adb.setIcon(android.R.drawable.ic_dialog_alert)

            adb.setPositiveButton(R.string.ok) {_, _ ->
                adapter?.deleteSelections()
                fillBookmarkData()
                onLongClick(0)
            }

            adb.setNegativeButton(R.string.cancel) { _, _ -> }
            adb.show()
        }

        return true
    }

    fun emptySelection() {
        adapter?.emptySelection()
        deleteMenu?.isVisible = false
    }

    override fun onLongClick(selectedCount: Int) {
        deleteMenu?.isVisible = (selectedCount > 0)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setDrawerState(selectedCount <= 0)
    }

    companion object {

        val FAVORITES_PAGE = "FAVORITES_PAGE"

        fun newInstance(page: Int): FavoritesPageFragment {
            val args = Bundle()
            args.putInt(FAVORITES_PAGE, page)
            val fragment = FavoritesPageFragment()
            fragment.arguments = args

            return fragment
        }
    }
}