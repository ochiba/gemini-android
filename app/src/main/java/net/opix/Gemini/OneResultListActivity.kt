package net.opix.Gemini

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.Menu

import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_one_result_list.*

import net.opix.database.Bookmark
import net.opix.database.Event
import net.opix.database.GeminiDatabaseHelper

import net.opix.database.QuickResult
import org.json.JSONArray
import org.json.JSONException
import java.util.ArrayList

class OneResultListActivity : GeminiActivity(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private lateinit var adapter: OneResultListAdaptor
    private var selectedEvent: Event? = null
    private var selectedAthlete: QuickResult? = null
    private var fromFavorites: Boolean = false
    internal var bTablet: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_result_list)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        selectedEvent = intent.getSerializableExtra("gemini_selected_event") as Event
        selectedAthlete = intent.getSerializableExtra("gemini_selected_athlete") as QuickResult
        fromFavorites = intent.getBooleanExtra("gemini_from_favorites", false)
        bTablet = intent.getBooleanExtra("gemini_is_tablet", false)

        if (selectedAthlete != null)
            title = selectedAthlete!!.firstName + " " + selectedAthlete!!.lastName

        adapter = OneResultListAdaptor(this, bTablet)
        list.adapter = adapter

        swipe_refresh_layout.setOnRefreshListener(this)

        swipe_refresh_layout.post {
            swipe_refresh_layout.isRefreshing = true

            fetchOneResult()
        }
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    override fun onRefresh() {
        fetchOneResult()
    }

    private fun fetchOneResult() {

        if (selectedAthlete == null)
            return

        swipe_refresh_layout.isRefreshing = true

        adapter.setAthleteName(selectedAthlete!!.firstName + " " + selectedAthlete!!.lastName)

        val extra = selectedAthlete!!.tableName + "&first_name=" + selectedAthlete!!.firstName + "&last_name=" + selectedAthlete!!.lastName + "&bib_number=" + selectedAthlete!!.id

        WebCoordinator.handleRequest(WebApiType.shareMessage, extra, this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_1_result, menu)

        val addMenu = menu.findItem(R.id.add_favorite)
        val deleteMenu = menu.findItem(R.id.delete_favorite)

        addMenu.isVisible = !fromFavorites
        deleteMenu.isVisible = fromFavorites
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val body = adapter.generateShareMessage(bTablet)

        when (item.itemId) {
           R.id.add_favorite -> {

               if (selectedEvent != null || selectedAthlete != null) {
                   if (GeminiDatabaseHelper(this).add1Favorite(selectedEvent!!, selectedAthlete!!))
                       showSnackbar(R.string.added_to_favorites, true)
               }
            }

            R.id.delete_favorite -> {
                val favorite = selectedAthlete as Bookmark?

                if (favorite != null) {

                    val adb = AlertDialog.Builder(this)

                    adb.setMessage(R.string.confirm_delete_one)
                    adb.setIcon(android.R.drawable.ic_dialog_alert)

                    adb.setPositiveButton(R.string.ok) {_, _ ->
                        if (GeminiDatabaseHelper(this).delete1Favorite(favorite.rowId.toString()))
                            onBackPressed()
                    }

                    adb.setNegativeButton(R.string.cancel) { _, _ -> }
                    adb.show()
                }
            }

            R.id.share -> {
                showSnackbar(R.string.copied_to_clipboard, true)
                GeminiApplication.instance!!.shareMessage(body)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun swipeRefreshLayout() {
        swipe_refresh_layout.isRefreshing = false
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {
        val allData = ArrayList<Any>()

        if (jArray.length() > 0) {
            try {
                val jArrayHeader = jArray.getJSONArray(0)
                val jArrayData = jArray.getJSONArray(1)

                var headerRow = TableRow()
                headerRow.SetType(TableRow.TYPE_3COL_HEADER)
                var dataRow = TableRow()

                for (j in 0 until jArrayHeader.length()) {

                    val header = jArrayHeader.getString(j)
                    val data = jArrayData.getString(j)

                    if (!(adapter.isAthleteInfo(header, data))) {

                        if (bTablet)
                            headerRow.Add(header)
                        else
                            dataRow.Add(header)

                        dataRow.Add(data)

                        if (bTablet && headerRow.IsAllFilled(bTablet)!! ||
                                (!bTablet) && dataRow.IsAllFilled(bTablet)!! ||
                                j == jArrayHeader.length() - 1) {

                            if (bTablet) {
                                allData.add(headerRow)

                                headerRow = TableRow()
                                headerRow.SetType(TableRow.TYPE_3COL_HEADER)
                            }

                            allData.add(dataRow)

                            dataRow = TableRow()
                        }
                    } else {
                        if (j == jArrayHeader.length() - 1) {
                            if (bTablet)
                                allData.add(headerRow)

                            allData.add(dataRow)
                        }
                    }
                }
            } catch (e: JSONException) {
                if (fromFavorites)
                    showToast("Please go to the results page and re-save this record.")
                else
                    showToast("Possibly a network error.  Please try later.")
            }
        }
        runOnUiThread {
            adapter.setRaceInfo(allData, selectedEvent!!.postTitle,
                    GeminiApplication.instance!!.getPostDate(selectedEvent!!.postDate),
                    selectedEvent!!.city + ", " + selectedEvent!!.state, selectedAthlete!!.raceTitle)
        }
    }
}