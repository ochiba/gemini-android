package net.opix.Gemini

import android.content.Intent
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.os.Bundle

import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_one_month_list.*

import net.opix.database.Event
import org.json.JSONArray
import java.util.ArrayList
import org.json.JSONException

class OneMonthListActivity : GeminiActivity(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private lateinit var adapter: EventListAdaptor
    private var eventYear = "2015"
    private var eventMonth = 1

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_month_list)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val extras = intent.extras

        if (extras != null) {

            eventMonth = extras.getInt("gemini_selected_month")
            eventYear = extras.getString("gemini_selected_year", "2015")

            when (eventMonth) {
                1 -> title = "January " + eventYear

                2 -> title = "February " + eventYear

                3 -> title = "March " + eventYear

                4 -> title = "April " + eventYear

                5 -> title = "May " + eventYear

                6 -> title = "June " + eventYear

                7 -> title = "July " + eventYear

                8 -> title = "August " + eventYear

                9 -> title = "September " + eventYear

                10 -> title = "October " + eventYear

                11 -> title = "November " + eventYear

                12 -> title = "December " + eventYear
            }
        }

        adapter = EventListAdaptor(this, ArrayList<Event>(), R.layout.gemini_one_month_layout)
        list.adapter = adapter

        swipe_refresh_layout.setOnRefreshListener(this)

        swipe_refresh_layout.post {
            swipe_refresh_layout.isRefreshing = true

            fetchEvents()
        }

        list.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val oneEvent = adapter.getItem(position) as Event

            val intent = Intent(this@OneMonthListActivity, RaceListActivity::class.java)

            intent.putExtra("gemini_selected_event", oneEvent)
            startActivity(intent)
        }
    }

    override fun onRefresh() {
        fetchEvents()
    }

    /**
     * Fetching movies json by making http call
     */
    private fun fetchEvents() {

        swipe_refresh_layout.isRefreshing = true

        if (eventYear.equals("2016", ignoreCase = true))
            eventYear = "0"

        val extra = "$eventYear&month=$eventMonth"
        WebCoordinator.handleRequest(WebApiType.events, extra,this)
    }

    override fun swipeRefreshLayout() {
        swipe_refresh_layout.isRefreshing = false
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {
        val newEventList = ArrayList<Event>()

        if (jArray.length() > 0) {

            for (i in 0 until jArray.length()) {
                try {
                    val oneObject = jArray.getJSONObject(i)

                    val one_event = Event(oneObject.getInt("ID"),
                            oneObject.getString("post_title"),
                            oneObject.getString("post_date"),
                            oneObject.getString("_grr_race_location_state"),
                            oneObject.getString("_grr_race_location_city"))

                    newEventList.add(one_event)
                } catch (e: JSONException) {
                    newEventList.clear()

                    runOnUiThread {
                        showToast("No events found")
                    }
                }
            }
        }

        runOnUiThread {
            if (jArray.length() == 0)
                showToast("No events found")

            adapter.refresh(newEventList)
        }
    }
}
