package net.opix.Gemini

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import android.widget.AdapterView
import android.widget.ArrayAdapter
import java.util.ArrayList

import kotlinx.android.synthetic.main.activity_grid_view.*

class MonthListActivity : AppCompatActivity() {
    var selectedYear: String? = null

    private var listValues: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_view)

        supportActionBar?.setDisplayShowHomeEnabled(true)

        val extras = intent.extras

        if (extras != null) {
            selectedYear = extras.getString("gemini_selected_year")
            title = selectedYear
        }

        listValues.add("January")
        listValues.add("February")
        listValues.add("March")
        listValues.add("April")
        listValues.add("May")
        listValues.add("June")
        listValues.add("July")
        listValues.add("August")
        listValues.add("September")
        listValues.add("October")
        listValues.add("November")
        listValues.add("December")

        grid_view.adapter = ArrayAdapter(this,
                R.layout.activity_single_row, R.id.display1, listValues)

        grid_view.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val intent = Intent(this@MonthListActivity, OneMonthListActivity::class.java)

            intent.putExtra("gemini_selected_year", selectedYear)
            intent.putExtra("gemini_selected_month", position + 1)
            startActivity(intent)
        }
    }
}
