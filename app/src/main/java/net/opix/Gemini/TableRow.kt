package net.opix.Gemini

class TableRow {


    var data1 = ""
    var data2 = ""
    var data3 = ""
    var type: Int = 0

    constructor() {
        this.type = TYPE_3COL_DATA

        this.data1 = ""
        this.data2 = ""
        this.data3 = ""
    }

    constructor(newType: Int, newData1: String, newData2: String, newData3: String) {
        this.type = newType
        this.data1 = newData1
        this.data2 = newData2
        this.data3 = newData3
    }

    constructor(newType: Int, newData1: String) {
        this.type = newType

        if (newType == TYPE_3COL_DATA_CENTER || newType == TYPE_RED_HEADER || newType == TYPE_BLACK_DATA) {
            this.data2 = newData1
            this.data1 = ""
        } else {
            this.data1 = newData1
            this.data2 = ""
        }
        this.data3 = ""
    }

    fun SetType(newType: Int) {
        this.type = newType
    }

    fun Add(newData1: String) {

        if (data1.isEmpty())
            this.data1 = newData1
        else if (data2.isEmpty())
            this.data2 = newData1
        else if (data3.isEmpty())
            this.data3 = newData1
    }

    fun IsAllFilled(bTablet: Boolean?): Boolean? {
        return if (bTablet!!)
            !this.data1.isEmpty() && !this.data2.isEmpty() && !this.data3.isEmpty()
        else
            !this.data1.isEmpty() && !this.data2.isEmpty()
    }

    companion object {

        val TYPE_3COL_DATA = 0
        val TYPE_3COL_HEADER = 1
        val TYPE_RED_HEADER = 2
        val TYPE_3COL_DATA_CENTER = 3
        val TYPE_BLACK_DATA = 4
    }
}
