package net.opix.Gemini

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff

import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView

import android.view.LayoutInflater
import android.view.ViewGroup

import java.util.TreeSet
import net.opix.database.Bookmark
import net.opix.database.Event
import net.opix.database.GeminiDatabaseHelper

class FavoriteListAdaptor(val activity: Activity, newList: List<Bookmark>, val isTablet: Boolean, val listener: FavoriteListAdaptor.OnFavoriteUserInteraction) : androidx.recyclerview.widget.RecyclerView.Adapter<FavoriteViewHolder>() {
    var bookmarkList = newList
    val selections = TreeSet<Int>()

    interface OnFavoriteUserInteraction {
        fun onLongClick(selectedCount: Int)
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val itemView = LayoutInflater.from(activity).inflate(R.layout.gemini_bookmark_layout, parent, false)
        return FavoriteViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return bookmarkList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (selections.contains(position)) TYPE_SELECTED else TYPE_NORMAL
    }

    fun getItem(location: Int): Bookmark {
        return bookmarkList[location]
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).rowId.toLong()
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {

        GeminiApplication.instance?.let {
            holder.post_date.text   = it.getPostDate(bookmarkList[position].postDate, "MMM d, yyyy")
        }

        holder.post_title.text  = bookmarkList[position].postTitle
        holder.athlete.text     =  String.format("%s %s", bookmarkList[position].firstName, bookmarkList[position].lastName)
        holder.race.text        = bookmarkList[position].raceTitle

        GeminiApplication.instance?.let {
            holder.overall.text = String.format("%s Overall", it.ordinalNumberFormat(bookmarkList[position].overall))
        }

        holder.profileColor.text = bookmarkList[position].raceTitle.replace(" ", "").substring(0, 2)
        val circle = ResourcesCompat.getDrawable(activity.resources, R.drawable.circle, null)
        val tint = Color.parseColor(bookmarkList[position].profileColor)

        circle!!.mutate().setColorFilter(tint, PorterDuff.Mode.SRC_IN)
        holder.profileColor.background = circle

        val type = getItemViewType(position)

        if (type == TYPE_SELECTED) {
            holder.container.setBackgroundColor(ResourcesCompat.getColor(activity.resources, R.color.colorPrimary, null))

            holder.post_date.setTextColor(Color.WHITE)
            holder.post_title.setTextColor(Color.WHITE)
            holder.athlete.setTextColor(Color.WHITE)
            holder.race.setTextColor(Color.WHITE)
            holder.overall.setTextColor(Color.WHITE)
        } else {
            holder.container.setBackgroundColor(if (position % 2 != 0) ResourcesCompat.getColor(activity.resources, R.color.lightLightGray, null) else Color.WHITE)

            holder.post_date.setTextColor(Color.DKGRAY)
            holder.athlete.setTextColor(Color.DKGRAY)
            holder.overall.setTextColor(Color.DKGRAY)
            holder.race.setTextColor(Color.DKGRAY)
        }

        val allElements = holder.getAllElements()

        for (oneElement in allElements) {
            oneElement.setOnClickListener {
                if (selectedCount() > 0) {
                    toggleSelection(position)
                    listener.onLongClick(selectedCount())
                }
                else
                    showDetails(position)
            }

            oneElement.setOnLongClickListener { _ ->
                toggleSelection(position)
                listener.onLongClick(selectedCount())
                true
            }
        }
    }

    private fun showDetails(position: Int) {
        val oneBookmark = bookmarkList[position]

        val intent = Intent(activity, OneResultListActivity::class.java)

        val selected_event = Event(oneBookmark.postId, oneBookmark.postTitle, oneBookmark.postDate, oneBookmark.state, oneBookmark.city)
        intent.putExtra("gemini_selected_athlete", oneBookmark)
        intent.putExtra("gemini_selected_event", selected_event)
        intent.putExtra("gemini_from_favorites", true)
        intent.putExtra("gemini_is_tablet", isTablet)

        activity.startActivity(intent)
    }

    private fun toggleSelection(position: Int) {
        if (selections.contains(position))
            selections.remove(position)
        else
            selections.add(position)

        notifyDataSetChanged()
    }

    fun emptySelection() {
        selections.clear()
        notifyDataSetChanged()
    }

    fun deleteSelections() {
        for (position in selections) {
            val favorite = bookmarkList[position]
            GeminiDatabaseHelper(activity).delete1Favorite(favorite.rowId.toString())
        }

        selections.clear()
    }

    companion object {

        private val TYPE_NORMAL = 0
        private val TYPE_SELECTED = 1
    }

    private fun selectedCount(): Int {
        return selections.size
    }
}
