package net.opix.Gemini

/**
 *
 * References:
 * 1) http://stackoverflow.com/questions/18302494/how-to-add-section-separators-dividers-to-a-listview
 * 2) http://stackoverflow.com/questions/13590627/android-listview-headers
 */

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import net.opix.database.Event

class MainListAdaptor(activity: Activity, EventList: ArrayList<Event>) : EventListAdaptor(activity, EventList, R.layout.gemini_year_event_layout) {

    private val yearList = ArrayList<String>()

    override fun getViewTypeCount(): Int {
        return 2
    }

    override fun getCount(): Int {

        return if (eventList.size > 0 && yearList.size > 0) eventList.size + yearList.size + 2 else 0

    } // one for "Latest Events", one between the 2 lists.

    fun getEventCount(): Int {
        return eventList.size
    }

    fun getYear(index: Int): String {

        if (index < 0 || index >= yearList.size)
            return ""

        return yearList[index]
    }

    fun getEvent(index: Int): Event {
        return eventList[index]
    }

    override fun getItem(location: Int): Any {

        if (location > 0 && location <= eventList.size)
            return eventList[location - 1]

        return if (location > eventList.size + 1) yearList[location - 2 - eventList.size] else 0
    }

    override fun getItemId(position: Int): Long {
        if (position > 0 && position <= eventList.size) {
            val oneEvent = getItem(position) as Event?
            return oneEvent?.id?.toLong() ?: 0

        } else if (position < yearList.size && position > eventList.size + 1) {
            val strYear = yearList[position - eventList.size - 2]
            try {
                return Integer.parseInt(strYear).toLong()
            } catch (nfe: NumberFormatException) {
            }
        }
        return 0
    }

    override fun getPost_Date(dateStr: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val cal = Calendar.getInstance()

        try {
            val dateObj = sdf.parse(dateStr)
            cal.time = dateObj

            val nSDateFormatterLongStyle = SimpleDateFormat("MMMM d, yyyy")
            return nSDateFormatterLongStyle.format(dateObj)
        } catch (e: ParseException) {
        }

        return ""
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var conView = convertView

        if (position > 0 && position <= eventList.size)
            return super.getView(position - 1, conView, parent)

        if (layoutInflater == null)
            layoutInflater = activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (conView == null)
            conView = layoutInflater!!.inflate(R.layout.gemini_year_event_layout, parent, false)

        val one_row = conView!!.findViewById<View>(R.id.post_date) as TextView
        one_row.gravity = Gravity.CENTER
        one_row.setTextColor(Color.WHITE)
        one_row.setShadowLayer(5f, 0f, 0f, Color.parseColor("#000000"))

        val post_title = conView.findViewById<View>(R.id.post_title) as TextView
        val post_city_state = conView.findViewById<View>(R.id.post_city_state) as TextView
        post_title.visibility = View.GONE
        post_city_state.visibility = View.GONE

        if (position == 0 || position == eventList.size + 1) {

            // Right now nothing between Latest Events and Years.
            if (position == 0)
                one_row.text = "Latest Events"
            else
                one_row.text = ""
        } else if (position > eventList.size + 1) {
            one_row.text = yearList[position - 2 - eventList.size]
        }

        conView.setBackgroundColor(Color.TRANSPARENT)
        return conView
    }

    fun refresh(newEventList : ArrayList<Event>, newYearList: ArrayList<String>) {
        eventList.clear()
        eventList.addAll(newEventList)

        yearList.clear()
        yearList.addAll(newYearList)

        notifyDataSetChanged()
    }
}
