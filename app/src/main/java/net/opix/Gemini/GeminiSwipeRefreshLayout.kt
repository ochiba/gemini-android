// Reference:
// https://stackoverflow.com/questions/35641630/recyclerviews-and-swiperefreshlayout-using-support-library-23-2-0/35643170#35643170

package net.opix.Gemini

import android.content.Context
import androidx.core.view.ViewCompat
import android.util.AttributeSet
import android.view.View
import android.widget.AbsListView

class GeminiSwipeRefreshLayout : androidx.swiperefreshlayout.widget.SwipeRefreshLayout {
    private var mScrollableChildId: Int = 0
    private var mScrollableChild: View? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        val a = context.obtainStyledAttributes(
                attrs, R.styleable.GeminiSwipeRefreshLayoutAttrs)
        mScrollableChildId = a.getResourceId(R.styleable.GeminiSwipeRefreshLayoutAttrs_scrollableChildId, 0)
        mScrollableChild = findViewById(mScrollableChildId)
        a.recycle()
    }

    override fun canChildScrollUp(): Boolean {
        ensureScrollableChild()

        if (android.os.Build.VERSION.SDK_INT < 14) {
            if (mScrollableChild is AbsListView) {
                val absListView = mScrollableChild as AbsListView?
                return absListView!!.childCount > 0 && (absListView.firstVisiblePosition > 0 || absListView.getChildAt(0)
                        .top < absListView.paddingTop)
            } else {
                return mScrollableChild!!.scrollY > 0
            }
        } else {
            return ViewCompat.canScrollVertically(mScrollableChild!!, -1)
        }
    }

    private fun ensureScrollableChild() {
        if (mScrollableChild == null) {
            mScrollableChild = findViewById(mScrollableChildId)
        }
    }
}