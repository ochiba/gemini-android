package net.opix.Gemini

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Menu

import android.view.MenuItem
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.util.Log
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView

import android.widget.PopupMenu
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast

import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest

import net.opix.database.Event
import net.opix.database.QuickResult
import org.json.JSONException

import java.io.UnsupportedEncodingException

import java.net.URLEncoder
import java.util.ArrayList
import java.util.Collections

import kotlinx.android.synthetic.main.activity_searchable.*

// How to determine screen size.  This is the only Activity that needs to know and decide how much of info can be displayed.
// http://alvinalexander.com/android/how-to-determine-android-screen-size-dimensions-orientation
// http://developer.android.com/guide/practices/screens_support.html

class SearchableActivity : GeminiActivity(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private val headerButtons = intArrayOf(R.id.header1, R.id.header2, R.id.header3, R.id.overall, R.id.header4, R.id.header5)

    private lateinit var adapter: SearchListAdapter
    private val GEMINI = SearchableActivity::class.java.getSimpleName()
    private var display1: String = "Chip"
    private var display2: String = ""
    private var display3: String = ""
    private var display4: String = ""
    private var display5: String = ""
    private var divisionList: MutableList<String> = ArrayList()
    private var tableName = ""
    private var currentKeyword: String = ""
    private var byDivision: Boolean = false
    private var bTop3: Boolean = false
    private var columnCount = COLUMN_COUNT_SMALL_SCREEN
    private var currentSortColumn = 0
    private var displayTitle = ""
    private var mSearchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchable)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val selectedEvent = intent.getSerializableExtra("gemini_selected_event") as Event
        tableName = intent.getStringExtra("gemini_selected_table_name")
        displayTitle = intent.getStringExtra("gemini_selected_display_name")

        title = displayTitle

        if (savedInstanceState != null) {
            display1 = savedInstanceState.getString("display1") ?: ""
            display2 = savedInstanceState.getString("display2") ?: ""
            display3 = savedInstanceState.getString("display3") ?: ""
            display4 = savedInstanceState.getString("display4") ?: ""
            display5 = savedInstanceState.getString("display5") ?: ""
            currentSortColumn = savedInstanceState.getInt("currentSortColumn")
            currentKeyword = savedInstanceState.getString("currentKeyword") ?: ""
            byDivision = savedInstanceState.getBoolean("byDivision")
            columnCount = savedInstanceState.getInt("columnCount")
            bTop3 = savedInstanceState.getBoolean("bTop3")
        }

        recyclerView.visibility = View.VISIBLE

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = layoutManager

        if (recyclerView.tag == "5")
            columnCount = COLUMN_COUNT_LARGE_SCREEN

        adapter = SearchListAdapter(this, ArrayList<QuickResult>(), selectedEvent)
        recyclerView.adapter = adapter

        swipe_refresh_layout.setOnRefreshListener(this)

        swipe_refresh_layout.post {
            swipe_refresh_layout.isRefreshing = true
            fetchDivision()
        }

        adapter.setColumnCount(columnCount)
    }

    // http://stackoverflow.com/questions/27156680/change-textcolor-in-searchview-using-android-toolbar
    // http://stackoverflow.com/questions/20121938/how-to-set-tint-for-an-image-view-programmatically-in-android
    private fun changeSearchViewTextColor(view: View?) {
        if (view != null) {

            if (view is TextView) {

                val textView = view as TextView?
                textView?.setTextColor(Color.WHITE)
                return
            } else if (view is ImageView) {
                val imageView = view as ImageView?
                imageView?.setColorFilter(Color.argb(255, 255, 255, 255)) // White Tint
                return
            } else if (view is ViewGroup) {
                val viewGroup = view as ViewGroup?
                
                for (i in 0 until viewGroup!!.childCount) {
                    changeSearchViewTextColor(viewGroup.getChildAt(i))
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_searchable, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        mSearchView = menu.findItem(R.id.action_search).actionView as SearchView
        // Assumes current activity is the searchable activity
        mSearchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        // int searchImgId = getResources().getIdentifier("android:id/search_edit", null, null);
        // Replace the default with custom search icon.  In 5.0, the default shows white box.
        // val searchImgId = resources.getIdentifier("android:id/search_button", null, null)

        mSearchView?.queryHint = resources.getString(R.string.type_name_or_bib)
        mSearchView?.isSubmitButtonEnabled = true

        // This did not work for the hint text color.  However, keep the code and idea.
        // changeSearchViewTextColor(mSearchView);

        mSearchView?.let {
            val id = it.context.resources.getIdentifier("android:id/search_src_text", null, null)
            val textView = it.findViewById<View>(id) as TextView
            textView.setTextColor(Color.WHITE)
            textView.setHintTextColor(Color.WHITE)
        }
        // At this point, plate (underline at the bottom of text in the searchView) is not hidden or changed its color yet.
        // http://dexpage.com/how-to-remove-white-underline-in-a-searchview-widget-in-toolbar-android/

        // Change the text color in the search box.  The Activity Bar has white but chnage for this case.
        //int id = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        //TextView textView = (TextView) mSearchView.findViewById(id);
        //textView.setTextColor(Color.WHITE);

        mSearchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {

                currentKeyword = query
                byDivision = false
                bTop3 = false
                fetchResults()

                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                // false if the SearchView should perform the default action of showing any suggestions if available, true if the action was handled by the listener.
                return false
            }
        })

        mSearchView?.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                mSearchView?.isIconified = true
            }
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        if (id == R.id.action_columns) {

            val intent = Intent(this@SearchableActivity, ColumnListActivity::class.java)

            intent.putExtra("gemini_selected_table_name", tableName)
            intent.putExtra("gemini_display1", display1)
            intent.putExtra("gemini_display2", display2)
            intent.putExtra("gemini_display3", display3)
            intent.putExtra("gemini_display4", display4)
            intent.putExtra("gemini_display5", display5)
            intent.putExtra("gemini_column_count", columnCount)

            startActivityForResult(intent, 0)
        }

        if (id == R.id.action_filter) {
            val menuItemView = findViewById<View>(id)
            val popup = PopupMenu(this@SearchableActivity, menuItemView)
            val top3 = resources.getString(R.string.top3)
            val noFilter = resources.getString(R.string.no_filter)
            val cancel = resources.getString(R.string.cancel)

            popup.menuInflater
                    .inflate(R.menu.menu_searchable_division, popup.menu)

            // Add No Filter
            // add (int groupId, int itemId, int order, CharSequence title)
            /*
            *   groupId	The group identifier that this item should be part of. This can be used to define groups of items for batch state changes. Normally use NONE if an item should not be in a group.
                itemId	Unique item ID. Use NONE if you do not need a unique ID.
                order	The order for the item. Use NONE if you do not care about the order. See getOrder().
                title	The text to display for the item.*/
            popup.menu.add(Menu.NONE, 1, 0, noFilter)
            popup.menu.add(Menu.NONE, 2, 1, top3)

            for (i in divisionList.indices)
                popup.menu.add(Menu.NONE, i + 3, i + 2, divisionList[i])

            // Add Cancel
            popup.menu.add(Menu.NONE, divisionList.size + 3, divisionList.size + 2, cancel)

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { menuItem ->
                // Cancel -> do nothing.
                if (menuItem.title.toString().equals(cancel, ignoreCase = true))
                    return@OnMenuItemClickListener true

                if (menuItem.title.toString().equals(noFilter, ignoreCase = true))
                    currentKeyword = ""
                else
                    currentKeyword = menuItem.title.toString()

                bTop3 = menuItem.title.toString().equals(top3, ignoreCase = true)
                byDivision = true
                fetchResults()

                true
            })
            popup.show() //showing popup menu
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null)
            return

        val newDisplay1 = data.getStringExtra("gemini_display1")
        val newDisplay2 = data.getStringExtra("gemini_display2")
        val newDisplay3 = data.getStringExtra("gemini_display3")
        val newDisplay4 = data.getStringExtra("gemini_display4")
        val newDisplay5 = data.getStringExtra("gemini_display5")

        // If nothing is selected, then do nothing.
        if (newDisplay1.isEmpty())
            return

        // If no change, then do nothing.
        if (display1.equals(newDisplay1, ignoreCase = true) &&
                display2.equals(newDisplay2, ignoreCase = true) &&
                display3.equals(newDisplay3, ignoreCase = true) &&
                display4.equals(newDisplay4, ignoreCase = true) &&
                display5.equals(newDisplay5, ignoreCase = true))
            return

        display1 = newDisplay1
        display2 = newDisplay2
        display3 = newDisplay3
        display4 = newDisplay4
        display5 = newDisplay5

        fetchResults()
    }

    override fun onRefresh() {
        fetchDivision()
    }

    private fun fetchDivision() {
        divisionList.clear()

        val url = "https://gemininext.com/mobile/?action=5&table=$tableName"

        val req1 = JsonArrayRequest(url,
                Response.Listener { jArray ->
                    Log.d(GEMINI, jArray.toString())

                    if (jArray.length() > 0) {

                        var bChipFieldExists = false

                        for (i in 0 until jArray.length()) {
                            try {
                                val oneObject = jArray.getJSONArray(i)

                                val oneDivision = oneObject.get(0) as String
                                divisionList.add(oneDivision)

                                if (!bChipFieldExists) {
                                    if (oneDivision.equals("Chip", ignoreCase = true))
                                        bChipFieldExists = true
                                }

                            } catch (e: JSONException) {
                                return@Listener
                            }
                        }

                        // The Chip field does not exist.  Determine what is used as Chip (Finish) time.
                        if (!bChipFieldExists) {
                            for (j in divisionList.indices) {
                                val oneDivision = divisionList[j]

                                if (oneDivision.equals("Gun", ignoreCase = true) || oneDivision.equals("Total Time", ignoreCase = true)) {
                                    display1 = oneDivision
                                    bChipFieldExists = true
                                    break
                                }
                            }
                        }
                    }
                    fetchResults()
                }, Response.ErrorListener { error ->
            Log.e(GEMINI, "Server Error: " + error.message)

            Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
            return@ErrorListener
        })

        req1.retryPolicy = DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        GeminiApplication.instance!!.addToRequestQueue(req1)
    }

    private fun fetchResults() {

        var url = ""
        currentSortColumn = 0

        try {
            // Only the keywords must be encoded, not the whole link.
            val tempoDisplay1 = URLEncoder.encode(display1, "UTF-8")
            val tempoDisplay2 = URLEncoder.encode(display2, "UTF-8")
            val tempoDisplay3 = URLEncoder.encode(display3, "UTF-8")
            val tempoDisplay4 = URLEncoder.encode(display4, "UTF-8")
            val tempoDisplay5 = URLEncoder.encode(display5, "UTF-8")

            val tempoCurrentKeyword = URLEncoder.encode(currentKeyword, "UTF-8")

            url = "https://gemininext.com/mobile/?action=2&table=" + tableName + "&key_word=" + tempoCurrentKeyword + "&exact=0&byDivision=" + (if (byDivision) 1 else 0) + "&displayColumn1=" + tempoDisplay1 + "&displayColumn2=" + tempoDisplay2 + "&displayColumn3=" + tempoDisplay3 + "&displayColumn4=" + tempoDisplay4 + "&displayColumn5=" + tempoDisplay5 + "&top3=" + if (bTop3) 1 else 0

            Log.d(GEMINI, "URL = $url")

        } catch (e: UnsupportedEncodingException) {
        }

        swipe_refresh_layout.isRefreshing = true

        val req = JsonArrayRequest(url,
                Response.Listener { jsonArray ->
                    Log.d(GEMINI, jsonArray.toString())

                    val newList = ArrayList<QuickResult>()

                    if (jsonArray.length() > 0) {
                        val na = resources.getString(R.string.na)

                        for (i in 0 until jsonArray.length()) {
                            try {
                                val one_record = jsonArray.getJSONArray(i)

                                val primaryKey = one_record.getInt(1)
                                val oneResult = QuickResult(primaryKey)
                                oneResult.overall = one_record.getInt(0)
                                oneResult.firstName = one_record.getString(2).replace("\\'", "'")
                                oneResult.lastName = one_record.getString(3).replace("\\'", "'")

                                // 3 more additional fields can be selected.
                                if (one_record.length() == 5)
                                    oneResult.display1 = one_record.getString(4)
                                else if (one_record.length() == 6) {
                                    oneResult.display1 = one_record.getString(4)
                                    oneResult.display2 = one_record.getString(5)
                                } else if (one_record.length() == 7) {
                                    oneResult.display1 = one_record.getString(4)
                                    oneResult.display2 = one_record.getString(5)
                                    oneResult.display3 = one_record.getString(6)
                                } else if (columnCount > 3) {
                                    if (one_record.length() == 8) {
                                        oneResult.display1 = one_record.getString(4)
                                        oneResult.display2 = one_record.getString(5)
                                        oneResult.display3 = one_record.getString(6)
                                        oneResult.display4 = one_record.getString(7)
                                    } else if (one_record.length() == 9) {
                                        oneResult.display1 = one_record.getString(4)
                                        oneResult.display2 = one_record.getString(5)
                                        oneResult.display3 = one_record.getString(6)
                                        oneResult.display4 = one_record.getString(7)
                                        oneResult.display5 = one_record.getString(8)
                                    }
                                }// determine Screen size
                                // http://alvinalexander.com/android/how-to-determine-android-screen-size-dimensions-orientation

                                // When sorted, 00:00:00 should not be at the top.
                                if (oneResult.display1 == "00:00:00")
                                    oneResult.display1 = na

                                if (oneResult.display2 == "00:00:00")
                                    oneResult.display2 = na

                                if (oneResult.display3 == "00:00:00")
                                    oneResult.display3 = na

                                if (oneResult.display4 == "00:00:00")
                                    oneResult.display4 = na

                                if (oneResult.display5 == "00:00:00")
                                    oneResult.display5 = na

                                oneResult.tableName = tableName
                                oneResult.raceTitle = displayTitle

                                newList.add(oneResult)
                            } catch (e: JSONException) {
                                runOnUiThread {
                                    showToast("Fail to download the results.  Please try again.")
                                }
                                newList.clear()
                            }
                        }
                    }

                    runOnUiThread {
                        adapter.refresh(newList)
                        setHeader()
                        swipe_refresh_layout.isRefreshing = false
                    }

                }, Response.ErrorListener { error ->
                Log.e(GEMINI, "Server Error: " + error.message)

                runOnUiThread {
                    if (error.message != null)
                        showToast(error.message!!)
                }

            swipe_refresh_layout.isRefreshing = false
        })

        GeminiApplication.instance!!.addToRequestQueue(req)
    }

    fun HeaderOnClick(v: View) {
        val oldSortColumn = currentSortColumn
        val newSortColumn = v.id
        var newAscending: Boolean? = true

        // Make sure the button's title is not empty.
        val button = findViewById<View>(newSortColumn) as HeaderButton
        val buttonTitle = button.text.toString()

        if (buttonTitle.isEmpty())
            return

        for (i in headerButtons.indices) {
            // The small screen does not have header 4 or 5.
            if (columnCount == COLUMN_COUNT_SMALL_SCREEN && i == COLUMN_COUNT_SMALL_SCREEN + 1)
                break

            val id = headerButtons[i]

            val header = findViewById<View>(id) as HeaderButton

            if (newSortColumn == id) {

                if (oldSortColumn == newSortColumn)
                    header.toggleAscending()
                else
                // Make bold.
                    header.setAscending(true, true)

                newAscending = header.ascending
            } else
            // Remove Bold from everything else.
            // http://stackoverflow.com/questions/6200533/set-textview-style-bold-or-italic
                header.setAscending(false, false)
        }
        currentSortColumn = newSortColumn

        val comparator = HeaderComparator()
        comparator.setSortIndex(newSortColumn)
        comparator.setAscending(newAscending)
        // Determine if a number column is clicked.  They contain "Place" such as "Division Place" or "Age".
        // If set to true, convert the data to integer.  If not converted, sorting does not work and looks like 1, 10, 11, 2, 3, etc. instead of 1, 2, 3, ... 10, 11, ...etc.
        comparator.setIsNumber(buttonTitle.contains("Place") || buttonTitle.equals("Age", ignoreCase = true))

        Collections.sort(adapter.getList(), comparator)

        adapter.notifyDataSetChanged()
    }

    private fun setHeader() {
        val headerCount = findViewById<View>(R.id.counter) as TextView

        var counter = adapter.itemCount.toString() + if (adapter.itemCount == 1) " Result" else " Results"

        if (!currentKeyword.isEmpty())
            counter += " (Keyword: $currentKeyword)"

        headerCount.text = counter

        val header1 = findViewById<View>(R.id.header1) as HeaderButton
        val header2 = findViewById<View>(R.id.header2) as HeaderButton
        val header3 = findViewById<View>(R.id.header3) as HeaderButton

        header1.setTextAndVisibility(display1)
        header2.setTextAndVisibility(display2)
        header3.setTextAndVisibility(display3)

        if (columnCount > 3) {
            val header4 = findViewById<View>(R.id.header4) as HeaderButton
            val header5 = findViewById<View>(R.id.header5) as HeaderButton

            header4.setTextAndVisibility(display4)
            header5.setTextAndVisibility(display5)
        }
    }

    // When orientation is changed, save these variables so the same data will be displayed.
    // http://stackoverflow.com/questions/5123407/losing-data-when-rotate-screen
    public override fun onSaveInstanceState(savedInstanceState: Bundle) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        savedInstanceState.putString("display1", display1)
        savedInstanceState.putString("display2", display2)
        savedInstanceState.putString("display3", display3)
        savedInstanceState.putString("display4", display4)
        savedInstanceState.putString("display5", display5)
        savedInstanceState.putInt("currentSortColumn", currentSortColumn)
        savedInstanceState.putString("currentKeyword", currentKeyword)
        savedInstanceState.putBoolean("byDivision", byDivision)
        savedInstanceState.putBoolean("bTop3", bTop3)
        savedInstanceState.putInt("columnCount", columnCount)

        super.onSaveInstanceState(savedInstanceState)
    }

    companion object {

        val COLUMN_COUNT_SMALL_SCREEN = 3
        val COLUMN_COUNT_LARGE_SCREEN = 5
    }
}