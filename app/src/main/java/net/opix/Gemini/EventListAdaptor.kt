package net.opix.Gemini

import android.app.Activity
import android.content.Context
import android.graphics.Color

import androidx.core.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.text.SimpleDateFormat

import java.util.Calendar
import java.util.Locale

import java.text.ParseException
import net.opix.database.Event

open class EventListAdaptor(val activity: Activity, var eventList: ArrayList<Event>, internal var layoutID: Int) : BaseAdapter() {
    var layoutInflater: LayoutInflater? = null

    override fun getCount(): Int {
        return eventList.size
    }

    override fun getItem(location: Int): Any {
        return eventList[location]
    }

    override fun getItemId(position: Int): Long {

        if (position < 0 || position >= getCount())
            return 0

        val one_event = getItem(position) as Event
        return one_event.id.toLong()
    }

    open fun getPost_Date(dateStr: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val cal = Calendar.getInstance()

        try {
            val dateObj = sdf.parse(dateStr)
            cal.time = dateObj
        } catch (e: ParseException) {
        }

        val dayOfMonth = cal.get(Calendar.DAY_OF_MONTH)

        return String.format("%02d", dayOfMonth)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var conView = convertView

        if (layoutInflater == null)
            layoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (conView == null)
            conView = layoutInflater!!.inflate(/*R.layout.gemini_one_month_layout*/layoutID, parent, false)

        val postTitle = conView!!.findViewById<View>(R.id.post_title) as TextView
        val postCityState = conView.findViewById<View>(R.id.post_city_state) as TextView

        if (position < 0 || position >= getCount())
            return conView
        
        // Change the color or row alternately.  This is not applied to the front page.
        if (R.layout.gemini_one_month_layout == layoutID) {
            conView.setBackgroundColor(if (position % 2 != 0) ResourcesCompat.getColor(activity.resources, R.color.lightLightGray, null) else Color.WHITE)

            val postDate = conView.findViewById<View>(R.id.post_date_calendar) as ImageView
            postDate.visibility = View.VISIBLE

            val uri = "@drawable/cal" + getPost_Date(eventList[position].postDate)

            val imageResource = activity.resources.getIdentifier(uri, null, activity.packageName)

            val resource = ResourcesCompat.getDrawable(activity.resources, imageResource, null)

            postDate.setImageDrawable(resource)
            postCityState.setTextColor(Color.DKGRAY)
        } else {
            val postDate = conView.findViewById<View>(R.id.post_date) as TextView
            postDate.text = getPost_Date(eventList[position].postDate)
            postDate.visibility = View.VISIBLE
        }

        postTitle.text = eventList[position].postTitle
        postCityState.text = eventList[position].city + ", " + eventList[position].state

        postTitle.visibility = View.VISIBLE
        postCityState.visibility = View.VISIBLE
        return conView
    }

    fun refresh(newList : ArrayList<Event>) {
        eventList.clear()
        eventList.addAll(newList)
        notifyDataSetChanged()
    }
}
