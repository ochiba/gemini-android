package net.opix.Gemini

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.core.content.res.ResourcesCompat

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import net.opix.database.Event
import net.opix.database.QuickResult
import java.util.ArrayList

class SearchListAdapter(private val activity: Activity, private val resultList: ArrayList<QuickResult>, private val selected_event: Event) : androidx.recyclerview.widget.RecyclerView.Adapter<SearchViewHolder>() {
    private var columnCount = 3

    fun setColumnCount(newCount: Int) {
        columnCount = newCount
    }

    fun getList() : ArrayList<QuickResult> {
        return resultList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val itemView = LayoutInflater.from(activity).inflate(R.layout.gemini_search_row_layout, parent, false)
        return SearchViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {

        if (getItemCount() == 0)
            return

        val one_result = resultList[position]

        holder.counter.text = (position + 1).toString()
        holder.overall.text = one_result.overall.toString()
        holder.name_bib.text = one_result.firstName + " " + one_result.lastName + " (#" + one_result.id + ")"
        holder.container.setBackgroundColor(if (position % 2 != 0) ResourcesCompat.getColor(activity.resources, R.color.lightLightGray, null) else Color.WHITE)

        holder.display1.text = one_result.display1
        holder.display2.text = one_result.display2
        holder.display3.text = one_result.display3

        if (columnCount > 3) {
            holder.enableDisplay4and5()
            holder.display4?.text = one_result.display4
            holder.display5?.text = one_result.display5
        }

        holder.container.setOnClickListener {
            val intent = Intent(activity, OneResultListActivity::class.java)

            intent.putExtra("gemini_selected_athlete", one_result)
            intent.putExtra("gemini_selected_event", selected_event)
            intent.putExtra("gemini_from_favorites", false)
            intent.putExtra("gemini_is_tablet", columnCount == SearchableActivity.COLUMN_COUNT_LARGE_SCREEN)
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    fun refresh(newList : ArrayList<QuickResult>) {
        resultList.clear()
        resultList.addAll(newList)
        notifyDataSetChanged()
    }
}
