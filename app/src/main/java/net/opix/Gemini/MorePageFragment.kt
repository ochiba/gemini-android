package net.opix.Gemini

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.ListView
import net.opix.database.GeminiSetting
import java.util.ArrayList

class MorePageFragment : GeminiFragment(), SettingListAdaptor.OnUserInteraction {
    private var mPage: Int = 0
    internal var mAdapter: SettingListAdaptor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPage = arguments!!.getInt(MORE_PAGE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_month_list, container, false)

        val listView = view.findViewById(android.R.id.list) as ListView

        val settings = ArrayList<GeminiSetting>()

        settings.add(GeminiSetting(requireActivity(), R.drawable.ic_photo, R.string.select_background_image, true, GeminiApplication.PREF_BACKGROUND_IMAGE))

        mAdapter = SettingListAdaptor(requireActivity(), settings, this)

        listView.adapter = mAdapter
        return view
    }

    override fun onSwitchChanged(position: Int, option: Int, isChecked: Boolean) {

        if (option == R.string.select_background_image) {

            if (isChecked)
                changeBackgroundImage()

            GeminiApplication.instance?.setUseOwnBackground(isChecked)
        }
    }

    override fun changeBackgroundImage() {

        val mainActivity = requireActivity() as MainActivity

        if (mainActivity != null)
            mainActivity.checkGalleryPermission(true)
    }

    companion object {

        val MORE_PAGE = "MORE_PAGE"

        fun newInstance(page: Int): MorePageFragment {
            val args = Bundle()
            args.putInt(MORE_PAGE, page)
            val fragment = MorePageFragment()
            fragment.arguments = args
            return fragment
        }
    }
}