package net.opix.Gemini

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.TypedValue
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import net.opix.Gemini.GeminiApplication.Companion.ALPHA_07
import org.json.JSONArray

open class GeminiActivity : AppCompatActivity(), WebCoordinator.WebCoordinatorDelegate {

    val layoutId: Int
        get() = R.id.swipe_refresh_layout

    fun showSnackbar(messageId: Int, good: Boolean) {
        val snackbar = Snackbar.make(findViewById(layoutId),
                messageId, Snackbar.LENGTH_SHORT)

        // get snackbar view
        val snackbarView = snackbar.view

        // change snackbar text color
        val snackbarTextId = com.google.android.material.R.id.snackbar_text
        val textView = snackbarView.findViewById<TextView>(snackbarTextId)
        textView.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary))
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.gemini_normal_font_size))

        snackbarView.setBackgroundColor(ContextCompat.getColor(this, if (good) R.color.colorPrimaryDark else R.color.colorPrimary))
        snackbarView.alpha = ALPHA_07
        snackbar.show()
    }

    fun CopyToClipboard(body: String) {

        val label = resources.getString(R.string.race_results)
        val manager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        manager.setPrimaryClip(ClipData.newPlainText(label, body))

        showSnackbar(R.string.copied_to_clipboard, true)
    }

    override fun showToast(errorMessage: String, context: Context?) {
        Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {}
    override fun swipeRefreshLayout() {}

    override fun showError(errorId: Int) {
        showSnackbar(errorId, false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}
