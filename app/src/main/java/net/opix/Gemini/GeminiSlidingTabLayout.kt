package net.opix.Gemini

import android.content.Context
import android.graphics.Color
import android.widget.TextView

/**
 * Created by osamuchiba on 7/10/15.
 */
class GeminiSlidingTabLayout(context: Context) : SlidingTabLayout(context, null) {

    override fun createDefaultTabView(context: Context): TextView {
        val textView = super.createDefaultTabView(context)
        textView.setAllCaps(false)
        textView.setTextColor(Color.WHITE)

        return textView
    }
}
