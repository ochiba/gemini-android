package net.opix.Gemini

/**
 * Created by osamuchiba on 6/16/15.
 *
 * References:
 * 1) http://stackoverflow.com/questions/18302494/how-to-add-section-separators-dividers-to-a-listview
 * 2) http://stackoverflow.com/questions/13590627/android-listview-headers
 */

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface

import androidx.core.content.res.ResourcesCompat
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import java.util.ArrayList

class OneResultListAdaptor(private val activity: Activity, newDeviceType: Boolean) : BaseAdapter() {
    private var inflater: LayoutInflater? = null
    private val na = activity.resources.getString(R.string.na)
    private val mData = ArrayList<Any>()

    private var athleteName = na
    private var gender = na
    private var city = ""
    private var state = ""
    private var age = ""
    private var country = na
    private var bib = na
    private var division = na
    private var divisionPlace = ""
    private var divisionTotal = ""
    private var totalRunners = ""
    private var genderTotal = ""
    private var genderPlace = ""
    private var overall = ""

    private var chip = ""
    private var gun = ""
    private var totalTime = ""

    private var isTablet = false

    internal class ViewHolder {
        var textView1: TextView? = null
        var textView2: TextView? = null
        var textView3: TextView? = null
    }

    fun setAthleteName(item: String) {
        athleteName = item
    }

    fun setRaceInfo(newData: ArrayList<Any>, newEventName: String, newEventDate: String, newEventLocation: String, raceTitle: String) {

        mData.clear()
        mData.addAll(newData)

        var dateLocation = newEventDate
        var third_row = raceTitle

        if (isTablet) {
            if (!dateLocation.equals("", ignoreCase = true))
                dateLocation += "   $newEventLocation"
            else
                dateLocation = newEventLocation
        } else
            third_row = newEventLocation

        mData.add(0, TableRow(TableRow.TYPE_3COL_DATA_CENTER, "", newEventName, ""))
        mData.add(1, TableRow(TableRow.TYPE_3COL_DATA_CENTER, "", dateLocation, ""))
        mData.add(2, TableRow(TableRow.TYPE_3COL_DATA_CENTER, "", third_row, ""))

        var hometown = city

        if (!hometown.equals("", ignoreCase = true) && !state.equals("", ignoreCase = true) && !state.equals(na, ignoreCase = true)) {
            hometown += ", $state"
        } else if (hometown.equals("", ignoreCase = true)) {
            hometown = state
        }

        if (!hometown.equals("", ignoreCase = true) && !country.equals("", ignoreCase = true) && !country.equals(na, ignoreCase = true)) {
            hometown += " $country"
        }

        if (hometown.equals("", ignoreCase = true)) {
            hometown = na
        }

        hometown = hometown.replace("United States", "US")

        if (isTablet) {
            mData.add(3, TableRow(TableRow.TYPE_RED_HEADER, "Athlete"))
            mData.add(4, TableRow(TableRow.TYPE_3COL_HEADER, "Name", "Age", "Gender"))
            mData.add(5, TableRow(TableRow.TYPE_3COL_DATA, athleteName, age, gender))
            mData.add(6, TableRow(TableRow.TYPE_3COL_HEADER, "Hometown", "Bib Number", "Division"))
            mData.add(7, TableRow(TableRow.TYPE_3COL_DATA, hometown, bib, division))
        } else {
            mData.add(3, TableRow(TableRow.TYPE_3COL_DATA_CENTER, "", raceTitle, ""))
            mData.add(4, TableRow(TableRow.TYPE_RED_HEADER, "Athlete"))
            mData.add(5, TableRow(TableRow.TYPE_3COL_DATA, "Name", athleteName, ""))
            mData.add(6, TableRow(TableRow.TYPE_3COL_DATA, "Age", age, ""))
            mData.add(7, TableRow(TableRow.TYPE_3COL_DATA, "Gender", gender, ""))
            mData.add(8, TableRow(TableRow.TYPE_3COL_DATA, "Hometown", hometown, ""))
            mData.add(9, TableRow(TableRow.TYPE_3COL_DATA, "Bib Number", bib, ""))
            mData.add(10, TableRow(TableRow.TYPE_3COL_DATA, "Division", division, ""))
        }
        mData.add(if (isTablet) 8 else 11, TableRow(TableRow.TYPE_RED_HEADER, "Official Time"))

        var officialTime = totalTime

        if (officialTime === "")
            officialTime = chip

        if (officialTime === "")
            officialTime = gun

        mData.add(if (isTablet) 9 else 12, TableRow(TableRow.TYPE_BLACK_DATA, "", officialTime, ""))
        mData.add(if (isTablet) 10 else 13, TableRow(TableRow.TYPE_RED_HEADER, "Rankings"))

        if (isTablet) {
            mData.add(11, TableRow(TableRow.TYPE_3COL_HEADER, "Overall", "Division", "Gender"))
            mData.add(12, TableRow(TableRow.TYPE_3COL_DATA, formatRanking(overall, totalRunners),
                    formatRanking(divisionPlace, divisionTotal),
                    formatRanking(genderPlace, genderTotal)))
        } else {
            mData.add(14, TableRow(TableRow.TYPE_3COL_DATA, "Overall", formatRanking(overall, totalRunners), ""))
            mData.add(15, TableRow(TableRow.TYPE_3COL_DATA, "Division", formatRanking(divisionPlace, divisionTotal), ""))
            mData.add(16, TableRow(TableRow.TYPE_3COL_DATA, "Gender", formatRanking(genderPlace, genderTotal), ""))
        }
        mData.add(if (isTablet) 13 else 17, TableRow(TableRow.TYPE_RED_HEADER, "Details"))
        notifyDataSetChanged()
    }

    private fun formatRanking(place: String, totalCount: String): String {
        var rank = place
        var total = totalCount

        if (rank.equals(na, ignoreCase = true) || rank.equals("", ignoreCase = true))
            rank = "0"

        if (total.equals(na, ignoreCase = true) || total.equals("", ignoreCase = true))
            total = "0"

        val nPlace = Integer.parseInt(rank)
        val nTotal = Integer.parseInt(total)

        if (nPlace == 0 && nTotal == 0) {
            return na
        } else if (nPlace != 0 && nTotal == 0) {
            return GeminiApplication.instance?.ordinalNumberFormat(nPlace) ?: na
        }

        return String.format("%s / %s", GeminiApplication.instance?.ordinalNumberFormat(nPlace) ?: "", total)
    }

    fun isAthleteInfo(section: String, data: String): Boolean {
        when (section.toUpperCase()) {
            "FIRST NAME", "LAST NAME" -> return true

            "AGE" -> {
                age = data
                return true
            }

            "CITY" -> {
                city = data
                return true
            }

            "COUNTRY" -> {
                country = data
                return true
            }

            "SEX" -> {
                gender = data
                return true
            }

            "STATE" -> {
                state = data
                return true
            }

            "BIB" -> {
                bib = data
                return true
            }

            "DIVISION" -> {
                division = data
                return true
            }

            "DIVISION PLACE" -> {
                divisionPlace = data
                return true
            }

            "DIVISION TOTAL" -> {
                divisionTotal = data
                return true
            }

            "TOTAL RUNNERS", "TOTAL COMPETITORS" -> {
                totalRunners = data
                return true
            }

            "GENDER TOTAL" -> {
                genderTotal = data
                return true
            }

            "SEX PLACE" -> {
                genderPlace = data
                return true
            }

            "OVERALL" -> {
                overall = data
                return true
            }

            "CHIP" -> {
                chip = data
                return true
            }

            "GUN" -> {
                gun = data
                return true
            }

            "TOTAL TIME" -> {
                totalTime = data
                return true
            }

            else -> {
            }
        }
        return false
    }

    init {
        isTablet = newDeviceType
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size > 0) {
            val row = mData[position] as TableRow
            return row.type
        }

        return TableRow.TYPE_3COL_DATA
    }

    override fun getViewTypeCount(): Int {
        return 5
    }

    override fun getCount(): Int {

        return mData.size
    }

    override fun getItem(position: Int): TableRow? {
        return if (mData.size > 0) {
            mData[position] as TableRow
        } else null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var conView = convertView

        var viewHolder = ViewHolder()

        if (inflater == null)
            inflater = activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val rowType = getItemViewType(position)
        val data = getItem(position) ?: return conView

        if (conView == null) {
            conView = inflater!!.inflate(R.layout.activity_3col_row, parent, false)
            viewHolder.textView1 = conView!!.findViewById<View>(R.id.display1) as TextView
            viewHolder.textView2 = conView.findViewById<View>(R.id.display2) as TextView
            viewHolder.textView3 = conView.findViewById<View>(R.id.display3) as TextView
            conView.tag = viewHolder
        } else
            viewHolder = conView.tag as ViewHolder

        // Reset always
        conView.setBackgroundColor(Color.WHITE)

        when (rowType) {
            TableRow.TYPE_RED_HEADER, TableRow.TYPE_3COL_HEADER, TableRow.TYPE_BLACK_DATA -> {

                setHeader(data, viewHolder)

                if (rowType == TableRow.TYPE_RED_HEADER)
                    conView.setBackgroundColor(ResourcesCompat.getColor(activity.resources, R.color.colorPrimary, null))
                else if (rowType == TableRow.TYPE_BLACK_DATA)
                    conView.setBackgroundColor(Color.BLACK)
                else
                    conView.setBackgroundColor(ResourcesCompat.getColor(activity.resources, R.color.lightLightGray, null))
            }

            else -> setData(data, viewHolder)
        }
        return conView
    }

    fun generateShareMessage(bTablet: Boolean): String {
        var content = "--- Event ---\n\n"

        if (!bTablet) {

            for (i in mData.indices) {

                val row = getItem(i)

                row?.let {
                    when (i) {
                        0, 1, 2, 3, 4 -> {

                            if (i == 4)
                                content += "\n--- "

                            content += it.data2

                            if (i == 4)
                                content += " ---\n"

                            content += "\n"
                        }

                        11, 13, 17 -> content += "\n--- " + it.data2 + " ---\n\n"

                        12 -> content += it.data2 + "\n"

                        else -> if (!it.data1.equals("", ignoreCase = true))
                            content += it.data1 + ":  " + it.data2 + "\n"
                    }
                }
            }
        } else {
            var i = 0
            while (i < mData.size) {

                val row = getItem(i)

                row?.let {
                    when (i) {
                        0, 1, 2, 3 -> {

                            if (i == 3)
                                content += "\n--- "

                            if (i == 1)
                                content += it.data2.replace("   ", "\n")
                            else
                                content += it.data2

                            if (i == 3)
                                content += " ---\n"

                            content += "\n"
                        }

                        8, 13 -> {
                            if (isTablet) {
                                content += "\n--- " + it.data2 + " ---\n\n"
                            }
                        }

                        9 -> {
                            if (isTablet) {
                                content += it.data2 + "\n"
                            }
                        }

                        10 -> {
                            if (!isTablet && i == 10) {
                                content += "\n--- " + it.data2 + " ---\n\n"
                            } else if (isTablet) {
                                content += "\n--- " + it.data2 + " ---\n\n"
                            }
                        }
                        else -> {
                            ++i
                            if (i < mData.size) {
                                val row2 = getItem(i)
                                content += generateMessageRows(it, row2)
                            }
                        }
                    }
                }
                i++
            }
        }
        return content
    }

    private fun setData(row: TableRow, viewHolder: ViewHolder) {
        viewHolder.textView1?.text = row.data1
        viewHolder.textView2?.text = row.data2
        viewHolder.textView3?.text = row.data3

        if (isTablet) {
            viewHolder.textView1?.setTypeface(Typeface.create(viewHolder.textView1?.typeface, Typeface.BOLD), Typeface.BOLD)
            viewHolder.textView3?.setTypeface(Typeface.create(viewHolder.textView3?.typeface, Typeface.BOLD), Typeface.BOLD)
        }

        viewHolder.textView2?.setTypeface(Typeface.create(viewHolder.textView2?.typeface, Typeface.BOLD), Typeface.BOLD)

        if (row.type == TableRow.TYPE_3COL_DATA_CENTER) {
            viewHolder.textView1?.visibility = TextView.GONE
            viewHolder.textView3?.visibility = TextView.GONE

        } else {
            viewHolder.textView1?.visibility = TextView.VISIBLE
            viewHolder.textView3?.visibility = if (isTablet) TextView.VISIBLE else TextView.GONE

            if (!isTablet) {
                viewHolder.textView1?.setTextColor(Color.DKGRAY)

                viewHolder.textView1?.gravity = Gravity.START
                viewHolder.textView2?.gravity = Gravity.END

                viewHolder.textView2?.setPadding(0, 0, 0, 0)
            }
        }
    }

    private fun setHeader(row: TableRow, viewHolder: ViewHolder) {
        viewHolder.textView1?.setTextColor(if (row.type == TableRow.TYPE_RED_HEADER || row.type == TableRow.TYPE_BLACK_DATA) Color.WHITE else Color.DKGRAY)
        viewHolder.textView2?.setTextColor(if (row.type == TableRow.TYPE_RED_HEADER || row.type == TableRow.TYPE_BLACK_DATA) Color.WHITE else Color.DKGRAY)

        viewHolder.textView1?.text = row.data1
        viewHolder.textView2?.text = row.data2

        viewHolder.textView1?.setTypeface(Typeface.create(viewHolder.textView1?.typeface, Typeface.NORMAL), Typeface.NORMAL)

        if (row.type == TableRow.TYPE_BLACK_DATA) {
            viewHolder.textView2?.setText(row.data2.replace("1".toRegex(), " 1"))
            viewHolder.textView2?.setTypeface(Typeface.createFromAsset(activity.assets, "fonts/TickingTimebombBB.ttf"), Typeface.NORMAL)
            viewHolder.textView2?.setTextSize(TypedValue.COMPLEX_UNIT_SP, 36f)
        } else
            viewHolder.textView2?.setTypeface(Typeface.create(viewHolder.textView2?.typeface, Typeface.NORMAL), Typeface.NORMAL)

        viewHolder.textView3?.setTypeface(Typeface.create(viewHolder.textView3?.typeface, Typeface.NORMAL), Typeface.NORMAL)

        if (isTablet) {
            viewHolder.textView3?.setTextColor(if (row.type == TableRow.TYPE_RED_HEADER || row.type == TableRow.TYPE_BLACK_DATA) Color.WHITE else Color.DKGRAY)
            viewHolder.textView3?.text = row.data3
        } else {
            viewHolder.textView3?.visibility = TextView.GONE

            if (row.type == TableRow.TYPE_RED_HEADER || row.type == TableRow.TYPE_BLACK_DATA)
                viewHolder.textView1?.visibility = TextView.GONE
        }
    }

    private fun generateMessageRows(row: TableRow, row2: TableRow?): String {
        var content = ""

        if (!row.data1.isEmpty() && row2 != null && !row2.data1.isEmpty()) {
            content = row.data1 + ": "
            content += row2.data1 + "\n"
        }

        if (!row.data2.isEmpty() && row2 != null && !row2.data2.isEmpty()) {
            content += row.data2 + ": "
            content += row2.data2 + "\n"
        }

        if (!row.data3.isEmpty() && row2 != null && !row2.data3.isEmpty()) {
            content += row.data3 + ": "
            content += row2.data3 + "\n"
        }

        return content
    }
}
