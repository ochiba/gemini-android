package net.opix.Gemini

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.charts.HorizontalBarChart

import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry

import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.Description
import org.json.JSONArray

import org.json.JSONException
import java.util.ArrayList

class ChartFragment : GeminiFragment(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    lateinit var fragment: View
    private var chart: HorizontalBarChart? = null

    private var selectedTable = ""
    private var selectedColumn = ""
    private var selectedType: Int = 0

    private val TYPE_DIVISION = 0
    private val TYPE_FINISHERS = 1

    private val yAxis = ArrayList<BarEntry>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        selectedTable = arguments?.getString("gemini_selected_table_name") ?: ""
        selectedType = arguments?.getInt("gemini_selected_type", 0) ?: 0
        selectedColumn = arguments?.getString("gemini_selected_column_name") ?: ""

        if (selectedType <= TYPE_FINISHERS)
            selectedColumn = ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragment = inflater.inflate(R.layout.gemini_chart_layout, container, false)

        chart = fragment.findViewById(R.id.chart) as HorizontalBarChart
        chart?.extraTopOffset = 10f
        chart?.extraBottomOffset = 10f

        val leg = chart?.legend
        leg?.xEntrySpace = 4f
        leg?.yOffset = 10f
        leg?.formSize = 8f
        leg?.textSize = 16f

        val desc = Description()
        desc.text = ""
        chart?.description = desc

        val rightAxis = chart?.axisRight
        rightAxis?.setDrawAxisLine(true)
        rightAxis?.setDrawGridLines(false)
        rightAxis?.textSize = 16f

        val leftAxis = chart?.axisLeft
        leftAxis?.setDrawAxisLine(true)
        leftAxis?.setDrawGridLines(true)
        leftAxis?.textSize = 16f

        val xAxis = chart?.xAxis
        xAxis?.position = XAxisPosition.BOTTOM
        xAxis?.textSize = 16f
        xAxis?.setDrawAxisLine(false)
        xAxis?.setDrawGridLines(false)
        xAxis?.xOffset = 10f

        fetchChart()

        return fragment
    }

    override fun onRefresh() {
        fetchChart()
    }

    private fun fetchChart() {
        // Default = TYPE_FINISHERS
        var extra = selectedTable

        if (TYPE_DIVISION != selectedType && !selectedColumn.isEmpty())
            extra += "&column=" + selectedColumn

        val chartType = if (TYPE_DIVISION == selectedType) WebApiType.byDivision else WebApiType.byFinisher

        WebCoordinator.handleRequest(chartType, extra,this)
    }

    private fun formatXValue(xValue: String, fiveMinutes: Boolean) //HH:mm:ss
            : String {
        val increment = if (fiveMinutes) 5 else 10
        val bumpTo1Hour = if (fiveMinutes) 55 else 50
        val stringArray = xValue.split(":".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

        var hours = Integer.parseInt(stringArray[0])
        var minutes = Integer.parseInt(stringArray[1])

        if (minutes >= bumpTo1Hour) {
            hours++
            minutes = 0
        } else {
            minutes += increment
            minutes = minutes / increment * increment
        }

        return String.format("%dh%02dm >", hours, minutes)
    }

    override fun handleResponse(jArray: JSONArray, type: WebApiType) {
        if (jArray.length() > 0) {
            val xArray = Array(jArray.length(), { _ -> ""})

            try {
                for (j in 0 until jArray.length()) {
                    val jArrayHeader = jArray.getJSONArray(j)

                    if (TYPE_DIVISION != selectedType)
                        xArray[j] = formatXValue(jArrayHeader.get(0) as String, !selectedColumn.isEmpty())
                    else {
                        var tempo = jArrayHeader.get(0) as String
                        tempo = tempo.replace("Female", "F")
                        tempo = tempo.replace("Male", "M")
                        tempo = tempo.replace("Military", "Mil")
                        tempo = tempo.replace("Clydesdale", "Cly")
                        tempo = tempo.replace("Athena", "Ath")
                        tempo = tempo.replace("Unknown", "?")
                        xArray[j] = tempo
                    }

                    yAxis.add(BarEntry(j.toFloat(), Integer.parseInt(jArrayHeader.get(1) as String).toFloat()))
                }

                val dataSet = BarDataSet(yAxis, "Number of Participants")
                dataSet.valueFormatter = GeminiYAxisFormatter()
                val data = BarData(dataSet)

                val barAxis = chart?.xAxis
                barAxis?.labelCount = jArray.length()
                barAxis?.valueFormatter = GeminiXAxisFormatter(xArray)
                chart?.data = data
                chart?.animateY(5000)

            } catch (e: JSONException) {
                showToast("No chart to show.  Please try again.", fragment.context)
            }
        }
        else
            showToast("No chart to show.  Please try again.", fragment.context)
    }

    companion object {

        fun newInstance(tableName: String, type: Int, column: String): ChartFragment {
            val args = Bundle()

            args.putString("gemini_selected_table_name", tableName)
            args.putInt("gemini_selected_type", type)
            args.putString("gemini_selected_column_name", column)

            val fragment = ChartFragment()
            fragment.arguments = args
            return fragment
        }
    }
}