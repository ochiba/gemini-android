package net.opix.Gemini

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.fragment.app.Fragment
import android.os.Parcelable

class ChartFragmentPagerAdapter(manager: androidx.fragment.app.FragmentManager, tabs: ArrayList<String>, newTableName: String) : androidx.fragment.app.FragmentStatePagerAdapter(manager) {

    private val tabTitles = tabs
    private val tableName = newTableName

    override fun getCount(): Int {
        return tabTitles.size
    }

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return ChartFragment.newInstance(tableName, position, tabTitles[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

    override fun saveState(): Parcelable? {
        return null
    }
}