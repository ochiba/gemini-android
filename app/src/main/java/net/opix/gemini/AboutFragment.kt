package net.opix.Gemini

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class AboutFragment : GeminiFragment() {
    private var adapter: AboutCardAdapter? = null
    private var mPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPage = arguments!!.getInt(AboutFragment.ABOUT_PAGE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_recycler_list, container, false)
        val listView = view!!.findViewById(R.id.recyclerView) as androidx.recyclerview.widget.RecyclerView
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(requireActivity())
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
        listView.layoutManager = layoutManager

        adapter = AboutCardAdapter(requireActivity())
        listView.adapter = adapter
        return view
    }

    companion object {

        val ABOUT_PAGE = "ABOUT_PAGE"

        fun newInstance(page: Int): AboutFragment {
            val args = Bundle()
            args.putInt(ABOUT_PAGE, page)
            val fragment = AboutFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
