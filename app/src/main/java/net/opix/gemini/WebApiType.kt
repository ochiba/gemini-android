package net.opix.Gemini

enum class WebApiType(val type: String) {

    years("1"),
    results("2&table="),
    division("5&table="),
    events("6&year=_"),
    subraces("7&post_id="),
    shareMessage("8&table="),
    columns("10&table="), // There are 2 types, with/out &time_only=1
    byDivision("15&table="),
    byFinisher("16&table="),
    randomNumber("17");

    fun url(extra: String = ""): String {
        return "https://gemininext.com/mobile/?action=" + this.type + extra
    }
}