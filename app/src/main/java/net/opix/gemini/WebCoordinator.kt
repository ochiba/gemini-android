package net.opix.Gemini

import android.content.Context
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import org.json.JSONArray
import org.json.JSONException

object WebCoordinator {

    private val tag = "WebCoordinator"

    interface WebCoordinatorDelegate {
        fun handleResponse(jArray: JSONArray, type: WebApiType)
        fun swipeRefreshLayout()
        fun showError(errorId: Int)
        fun showToast(errorMessage: String, context: Context? = null)
    }

    fun handleRequest(type: WebApiType, extra: String, delegate: WebCoordinatorDelegate) {

        val url = type.url(extra)

        try {
            val req = JsonArrayRequest(url,
                Response.Listener { jArray ->
                    Log.d(tag, jArray.toString())

                    delegate.handleResponse(jArray, type)

                    delegate.swipeRefreshLayout()
                }, Response.ErrorListener { error ->
                    Log.e(tag, "Server Error: " + error.message)

                    if (error.message != null)
                        delegate.showToast(error.message!!)

                    delegate.swipeRefreshLayout()
                })

                GeminiApplication.instance!!.addToRequestQueue(req)
        } catch (e: JSONException) {
            Log.e(tag, "handleRequest", e)
            delegate.swipeRefreshLayout()
        }
    }
}