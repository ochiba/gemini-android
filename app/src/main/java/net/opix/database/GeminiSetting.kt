package net.opix.database

import android.content.Context
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import net.opix.Gemini.GeminiApplication

data class GeminiSetting(private val mContext: Context, var mImageId: Int, var mTitle: Int, var mUseSwitch: Boolean, var sharedPrefId: String) : BaseObservable() {
    var choice: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                val sharedPref = mContext.getSharedPreferences(GeminiApplication::class.java.simpleName, Context.MODE_PRIVATE)

                val editor = sharedPref.edit()
                editor.putBoolean(sharedPrefId, choice)
                editor.commit()
            }
        }

    init {
        this.choice = false

        if (mUseSwitch && !sharedPrefId.isEmpty()) {
            val sharedPref = mContext.getSharedPreferences(GeminiApplication::class.java.simpleName, Context.MODE_PRIVATE)

            this.choice = sharedPref.getBoolean(sharedPrefId, false)
        }
    }

    @Bindable
    fun getChoice(): Boolean? {
        return choice
    }
}
