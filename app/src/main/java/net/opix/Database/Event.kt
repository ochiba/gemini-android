package net.opix.database

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

import java.io.Serializable

// http://stackoverflow.com/questions/2736389/how-to-pass-object-from-one-activity-to-another-in-android
/**
 * Created by osamuchiba on 6/11/15.
 */

data class Event(var id: Int, @get:Bindable
var postTitle: String, @get:Bindable
            var postDate: String, @get:Bindable
            var state: String, @get:Bindable
            var city: String) : BaseObservable(), Serializable {
    var post_status: String
    var post_modified: String

    init {
        this.post_status = ""
        this.post_modified = ""
    }
}
