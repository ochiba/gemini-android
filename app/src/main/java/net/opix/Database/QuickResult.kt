package net.opix.database

import java.io.Serializable

open class QuickResult(var id: Int) : Serializable {
    var overall: Int = 0
    var chipTime: String
    var tableName: String
    var firstName: String
    var lastName: String
    var raceTitle: String
    var display1: String
    var display2: String
    var display3: String
    var display4: String
    var display5: String

    init {
        this.raceTitle = ""
        this.chipTime = ""
        this.lastName = ""
        this.firstName = ""
        this.tableName = ""
        this.display1 = ""
        this.display2 = ""
        this.display3 = ""
        this.display4 = ""
        this.display5 = ""
        this.overall = 0
    }

    fun getBibNumber(): Int {
        return id
    }

    fun setBibNumber(newID: Int) {
        this.id = newID
    }
}
