package net.opix.database

data class EventSubrace(var ID: Int, var post_id: Int, var display_name: String, var table_name: String)