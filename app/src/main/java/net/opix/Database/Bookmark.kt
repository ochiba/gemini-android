package net.opix.database

import java.io.Serializable

data class Bookmark(var rowId: Int) : QuickResult(rowId), Serializable {
    var postId: Int = 0
    var postTitle: String
    var postDate: String
    var city: String = ""
    var state: String = ""
    var profileColor: String

    init {
        this.postId = 0
        postTitle = ""
        postDate = "0000-00-00 00:00:00"
        profileColor = ""
    }
}