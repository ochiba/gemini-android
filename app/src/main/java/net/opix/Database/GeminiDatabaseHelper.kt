package net.opix.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import net.opix.Gemini.GeminiApplication
import java.util.*

//import android.support.v4.media.routing.MediaRouterJellybean;

class GeminiDatabaseHelper(val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {

        db.execSQL("CREATE TABLE IF NOT EXISTS [Events] ([ID] INTEGER PRIMARY KEY  NOT NULL UNIQUE, [post_date] TEXT default '0000-00-00 00:00:00', [post_title] TEXT NOT NULL, [city] TEXT  default '', [state] TEXT  default 'CA', [post_status] TEXT default 'publish', [post_modified] TEXT default '0000-00-00 00:00:00');")
        db.execSQL("CREATE TABLE IF NOT EXISTS [Bookmarks] ([ID] INTEGER PRIMARY KEY AUTOINCREMENT, [post_id] INTEGER DEFAULT 0, [race] TEXT DEFAULT '', [Table_Name] TEXT DEFAULT '', [Bib_Number] INTEGER DEFAULT 0, [Overall] INTEGER DEFAULT 0, [First_Name] TEXT DEFAULT '', [Last_Name] TEXT DEFAULT '', [Follow_ID] INTEGER DEFAULT 0, [Profile_Color] TEXT DEFAULT '#673AB7');")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db)

        val cursor = db.rawQuery("PRAGMA table_info(Bookmarks)", null)

        cursor.moveToFirst()

        var foundFollowId = false
        var foundProfileColor = false

        while (!cursor.isAfterLast) {
            val column = cursor.getString(1)

            if (column.equals("Follow_ID", ignoreCase = true))
                foundFollowId = true

            if (column.equals("Profile_Color", ignoreCase = true))
                foundProfileColor = true

            cursor.moveToNext()
        }

        if (!foundFollowId)
            db.execSQL("ALTER TABLE [Bookmarks] ADD COLUMN [Follow_ID] INTEGER DEFAULT 0")

        if (!foundProfileColor)
            db.execSQL("ALTER TABLE [Bookmarks] ADD COLUMN [Profile_Color] TEXT DEFAULT ''")
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        val DATABASE_VERSION = 9
        val DATABASE_NAME = "Gemini.db"
    }

    fun getFavorites(): ArrayList<Bookmark> {
        var bookmarkList = ArrayList<Bookmark>()

        try {
            val db = readableDatabase
            val sql = "SELECT Bookmarks.ID, Bookmarks.post_id, Bookmarks.race, Bookmarks.Table_Name, Bookmarks.Bib_Number, Bookmarks.Overall, Bookmarks.First_Name, Bookmarks.Last_Name, Events.post_title, Events.post_date, Events.city, Events.state, Bookmarks.Profile_Color FROM Bookmarks INNER JOIN Events ON Bookmarks.post_id=Events.ID ORDER BY Bookmarks.ID DESC"
            val cursor = db.rawQuery(sql, null)

            cursor.moveToFirst()

            while (!cursor.isAfterLast) {
                val id = cursor.getInt(0)

                val one_bookmark = Bookmark(id)
                one_bookmark.postId = cursor.getInt(1)
                one_bookmark.raceTitle = cursor.getString(2)
                one_bookmark.tableName = cursor.getString(3)
                one_bookmark.id = cursor.getInt(4)
                one_bookmark.overall = cursor.getInt(5)
                one_bookmark.firstName = cursor.getString(6)
                one_bookmark.lastName = cursor.getString(7)
                one_bookmark.postTitle = cursor.getString(8)
                one_bookmark.postDate = cursor.getString(9)
                one_bookmark.city = cursor.getString(10)
                one_bookmark.state = cursor.getString(11)
                one_bookmark.profileColor = cursor.getString(12)

                if (one_bookmark.profileColor.isEmpty()) {
                    one_bookmark.profileColor = update1FavoriteColor(id, one_bookmark.raceTitle)

                    if (one_bookmark.profileColor.isEmpty())
                        one_bookmark.profileColor = "#2196F3"
                }

                bookmarkList.add(one_bookmark)
                cursor.moveToNext()
            }
            db.close()
        } catch (e: Exception) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
        }

        return bookmarkList
    }

    fun delete1Favorite(id: String): Boolean {

        val selectionArgs = arrayOf(id)
        val selection = "ID = ?"

        try {

            val db = writableDatabase
            db.beginTransaction()
            db.delete("Bookmarks", selection, selectionArgs)
            db.setTransactionSuccessful()
            db.endTransaction()
            db.close()
        } catch (e: SQLiteException) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    fun add1Favorite(event: Event, athlete: QuickResult): Boolean {

        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put("ID", event.id)
        values.put("post_title", event.postTitle)
        values.put("post_date", event.postDate)
        values.put("city", event.city)
        values.put("state", event.state)

        try {
            db.beginTransaction()

            db.insertWithOnConflict(
                    "Events", null,
                    values,
                    SQLiteDatabase.CONFLICT_IGNORE)

            values.clear()
            db.setTransactionSuccessful()
            db.endTransaction()
        } catch (e: SQLiteException) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            db.close()
            return false
        }

        values.put("post_id", event.id)
        values.put("race", athlete.raceTitle)
        values.put("Table_Name", athlete.tableName)
        values.put("Bib_Number", athlete.getBibNumber())
        values.put("Overall", athlete.overall)
        values.put("First_Name", athlete.firstName)
        values.put("Last_Name", athlete.lastName)

        val color = GeminiApplication().getBackgroundColor(context, athlete.raceTitle)

        values.put("Profile_Color", color)

        try {
            db.beginTransaction()
            db.insertOrThrow(
                    "Bookmarks", null,
                    values)
            db.setTransactionSuccessful()
            db.endTransaction()
            db.close()
        } catch (e: SQLiteException) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    fun update1FavoriteColor(id: Int, race: String): String {

        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        val color = GeminiApplication().getBackgroundColor(context, race)
        values.put("Profile_Color", color)

        val whereArgs = arrayOf(id.toString())
        val where = "ID = ?"

        try {
            db.beginTransaction()
            db.update("Bookmarks", values, where, whereArgs)
            db.setTransactionSuccessful()
            db.endTransaction()
            db.close()
        } catch (e: SQLiteException) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            return ""
        }
        return color
    }
}